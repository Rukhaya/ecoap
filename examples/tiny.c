/*
The MIT License (MIT)

Copyright (c) 2015 José Bollo <jobol@nonadev.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include "ecoap.h"
    
void put_unsigned_content(unsigned int value) {
  uint8_t buf[20];
  uint8_t len = 0;

  while (value) {
    buf[20-++len] = (uint8_t)value;
    value >>= 8;
  }
#if 1
  buf[19-len] = len;
  ecoap_put_content(buf+19-len, len+1);
#else
  ecoap_put_content(buf+20-len, len);
#endif
}


int main(int argc, char **argv) {
#if 1
  struct sockaddr_in  destination4;
#else
  struct sockaddr_in6 destination6;
#endif

  if ( argc > 1 && strcmp(argv[1], "-h") == 0 ) {
    char *base = strrchr(argv[0], '/');
    printf("usage: %s [destination]\n", base ? base : argv[0]);
    return !!strcmp(argv[1], "-h");
  }

#if 1
  memset(&destination4, 0, sizeof destination4);
  destination4.sin_family = AF_INET;
  inet_pton( AF_INET, argc > 1 ? argv[1] : "127.0.0.1", &destination4.sin_addr );
  destination4.sin_port = htons( eCoAP_DEFAULT_COAP_PORT );
#else
  memset(&destination6, 0, sizeof destination6);
  destination6.sin6_family = AF_INET6;
  inet_pton( AF_INET6, argc > 1 ? argv[1] : "::1", &destination6.sin6_addr );
  destination6.sin6_port = htons( eCoAP_DEFAULT_COAP_PORT );
#endif

  ecoap_init();
  ecoap_interface_add_default(true, false);
  ecoap_trace(stderr, "*");  
  for(;;) {
    ecoap_request_post();
    put_unsigned_content(rand() >> (rand() & 0x1f));
#if 1
    ecoap_put_destination((struct sockaddr*)&destination4, sizeof destination4);
#else
    ecoap_put_destination((struct sockaddr*)&destination6, sizeof destination6);
#endif
    ecoap_send();
    sleep(5);
    ecoap_wait_max(0);
    ecoap_dispatch_events();
  }

  return 0;
}



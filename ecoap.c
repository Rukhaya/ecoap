/*
The MIT License (MIT)

Copyright (c) 2015 José Bollo <jobol@nonadev.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <features.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <netdb.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include "ecoap.h"

/********************* SECTION: tuning ***********************************/

#if !defined(ECOAP_DEBUG_MEMORY)
# define ECOAP_DEBUG_MEMORY  1
#endif

#if !defined(ECOAP_TRACE)
# define ECOAP_TRACE  1
#endif

#if !defined(ECOAP_SALT)
# define ECOAP_SALT  1
#endif

#if !defined(ECOAP_SALT_MESSAGE_ID)
# define ECOAP_SALT_MESSAGE_ID  ECOAP_SALT
#endif

#if !defined(ECOAP_SALT_TOKEN)
# define ECOAP_SALT_TOKEN  ECOAP_SALT
#endif

#if !defined(ECOAP_COMPRESS_RETRANSMIT)
# define ECOAP_COMPRESS_RETRANSMIT  1
#endif

#if !defined(ECOAP_TIME_APPROX)
# define ECOAP_TIME_APPROX  2
#endif

#if !defined(ECOAP_REENTRANT_DISPATCH)
# define ECOAP_REENTRANT_DISPATCH 1
#endif

/********************* SECTION: assertion helper *************************/

#if !defined(NDEBUG)
#  define ifassert(x) x
#else
#  define ifassert(x)
#endif

/********************* SECTION: multi-threading **************************/

#if defined(_REENTRANT) || defined(_THREAD_SAFE)
# include <pthread.h>
# define BARRIER(x) static pthread_mutex_t _mutex_for_##x##_ = PTHREAD_MUTEX_INITIALIZER;
# define LOCK(x)    do{ifassert(int sts=)pthread_mutex_lock(&_mutex_for_##x##_);assert(sts==0);}while(0);
# define UNLOCK(x)  do{ifassert(int sts=)pthread_mutex_unlock(&_mutex_for_##x##_);assert(sts==0);}while(0);
#else
# define __thread
# define BARRIER(x)
# define LOCK(x)
# define UNLOCK(x)
#endif

/********************* SECTION: internal eCoAP constants *****************/

/* minimal message length */
#define eCoAP_MINIMAL_MESSAGE_LENGTH   4

/* field VERSION */
#define eCoAP_VERSION_INDEX            0
#define eCoAP_VERSION_SHIFT            6
#define eCoAP_VERSION_MASK             3

#define eCoAP_VERSION_1                1

/* field TYPE */
#define eCoAP_TYPE_INDEX               0
#define eCoAP_TYPE_SHIFT               4
#define eCoAP_TYPE_MASK                3

#define eCoAP_TYPE_CONFIRMABLE         0
#define eCoAP_TYPE_NON_CONFIRMABLE     1
#define eCoAP_TYPE_ACKNOWLEDGMENT      2
#define eCoAP_TYPE_RESET               3

/* field TOKEN_LENGTH */
#define eCoAP_TOKEN_LENGTH_INDEX       0
#define eCoAP_TOKEN_LENGTH_SHIFT       0
#define eCoAP_TOKEN_LENGTH_MASK        15

#define eCoAP_TOKEN_LENGTH_MINIMUM     0
#define eCoAP_TOKEN_LENGTH_MAXIMUM     8

/* field CODE */
#define eCoAP_CODE_INDEX               1
#define eCoAP_CODE_SHIFT               0
#define eCoAP_CODE_MASK                255

/* field MESSAGE_ID */
#define eCoAP_MESSAGE_ID_INDEX         2
#define eCoAP_MESSAGE_ID_INDEX_HIGH    2
#define eCoAP_MESSAGE_ID_INDEX_LOW     3

/* offset of the token */
#define eCoAP_TOKEN_INDEX              4

/* handling options */
#define eCoAP_OPTION_INITIAL           0

#define eCoAP_OPTION_DELTA_SHIFT       4
#define eCoAP_OPTION_DELTA_MASK        15

#define eCoAP_OPTION_LENGTH_SHIFT      0
#define eCoAP_OPTION_LENGTH_MASK       15

#define eCoAP_OPTION_EXT1_VALUE        13
#define eCoAP_OPTION_EXT1_OFFSET       13

#define eCoAP_OPTION_EXT2_VALUE        14
#define eCoAP_OPTION_EXT2_OFFSET       269

#define eCoAP_OPTION_BAD_VALUE         15
#define eCoAP_OPTION_IS_END(x)         (!(uint8_t)(~((uint8_t)(x))))

#define eCoAP_OPTION_CRITICAL_MASK     1
#define eCoAP_OPTION_CRITICAL          1

#define eCoAP_OPTION_UNSAFE_MASK       2
#define eCoAP_OPTION_UNSAFE            2

#define eCoAP_OPTION_NO_CACHE_MASK     30
#define eCoAP_OPTION_NO_CACHE_VALUE    28

/* content data separator */
#define eCoAP_CONTENT_INIT             255

/* transmission parameters */
#define eCoAP_MAX_RETRANSMIT           4
#define eCoAP_NSTART                   1

#define eCoAP_ACK_TIMEOUT_MIN_256      512        /* 1/256° second */
#define eCoAP_ACK_TIMEOUT_MAX_256      768        /* 1/256° second */
#define eCoAP_MAX_LATENCY_256          25600      /* 1/256° second */
#define eCoAP_DEFAULT_LEISURE_256      1280       /* 1/256° second */
#define eCoAP_PROBING_RATE             1          /* byte/second */

#define eCoAP_MAX_TRANSMIT_SPAN_256    (((eCoAP_ACK_TIMEOUT_MAX_256) << (eCoAP_MAX_RETRANSMIT)) - (eCoAP_ACK_TIMEOUT_MAX_256)) /* 1/256° second */
#define eCoAP_MAX_TRANSMIT_WAIT_256    (((eCoAP_ACK_TIMEOUT_MAX_256) << (eCoAP_MAX_RETRANSMIT + 1)) - (eCoAP_ACK_TIMEOUT_MAX_256)) /* 1/256° second */
#define eCoAP_PROCESSING_DELAY_256     eCoAP_ACK_TIMEOUT_MIN_256 /* 1/256° second */
#define eCoAP_MAX_RTT_256              ((2 * eCoAP_MAX_LATENCY_256) + eCoAP_PROCESSING_DELAY_256)     /* 1/256° second */
#define eCoAP_EXCHANGE_LIFETIME_256    (eCoAP_MAX_TRANSMIT_SPAN_256 + (2 * eCoAP_MAX_LATENCY_256) + eCoAP_PROCESSING_DELAY_256)    /* 1/256° second */
#define eCoAP_NON_LIFETIME_256         (eCoAP_MAX_TRANSMIT_SPAN_256 + eCoAP_MAX_LATENCY_256)          /* 1/256° second */

#define _inverse_of_256_               0.00390625   /* == 1/256 */

#define eCoAP_ACK_TIMEOUT              (eCoAP_ACK_TIMEOUT_MIN_256 * _inverse_of_256_)
#define eCoAP_ACK_RANDOM_FACTOR        (eCoAP_ACK_TIMEOUT_MAX_256.0 / eCoAP_ACK_TIMEOUT_MIN_256.0)
#define eCoAP_DEFAULT_LEISURE          (eCoAP_DEFAULT_LEISURE_256 * _inverse_of_256_)   /* second */
#define eCoAP_MAX_TRANSMIT_SPAN        (eCoAP_MAX_TRANSMIT_SPAN_256 * _inverse_of_256_) /* second */
#define eCoAP_MAX_TRANSMIT_WAIT        (eCoAP_MAX_TRANSMIT_WAIT_256 * _inverse_of_256_) /* second */
#define eCoAP_MAX_LATENCY              (eCoAP_MAX_LATENCY_256 * _inverse_of_256_)       /* second */
#define eCoAP_PROCESSING_DELAY         (eCoAP_PROCESSING_DELAY_256 * _inverse_of_256_)  /* second */
#define eCoAP_EXCHANGE_LIFETIME        (eCoAP_EXCHANGE_LIFETIME_256 * _inverse_of_256_) /* second */
#define eCoAP_NON_LIFETIME             (eCoAP_NON_LIFETIME_256 * _inverse_of_256_)      /* second */

/********************* SECTION: local constants **************************/

/* this defines the maximum length of tokens */
#define GENERATED_TOKEN_LENGTH  3   /* #bytes */

/* this defines the size for receiving */
#define RECEIVE_SIZE  3000

/********************* SECTION: helpers **********************************/

static uint16_t _strlen_(const char *value)
{
  size_t length = strlen(value);
  assert((length >> 16) == 0);
  return (uint16_t)length;
}

/********************* SECTION: tracing **********************************/

#if ECOAP_TRACE
enum {
  trace_sent = 1,
  trace_received = 2,
  trace_memory = 4
};
static const char _trace_names_[] = "sent,received,memory";
static uint32_t _trace_flags_ = 0;
static FILE *_trace_file_ = NULL;

static bool _traces_(uint32_t mask)
{
  return _trace_file_!=NULL && (_trace_flags_ & mask) != 0;
}

void ecoap_trace(FILE *file, const char *desc)
{
  int ldesc, lname, remove = 0;
  uint32_t mask;
  const char *name;

  _trace_file_ = file;
  _trace_flags_ = 0;
  if (desc == NULL)
    return;

  for(;;) {
    switch (*desc) {
    case 0:
      return;
    case '+':
      remove = 0;
      desc++;
      break;
    case '-':
      remove = 1;
      desc++;
      break;
    case ',':
      desc++;
      break;
    case '*':
      _trace_flags_ = remove ? 0 : 0xffffffff;
      desc++;
      break;
    default:
      for(ldesc = 0 ; desc[ldesc] != 0 && desc[ldesc] != ',' && desc[ldesc] != '+' && desc[ldesc] != '-' ; ldesc++);
      mask = 1;
      name = _trace_names_;
      while (name) {
        for(lname = 0 ; name[lname] != 0 && name[lname] != ',' ; lname++);
        if (ldesc == lname && !memcmp(desc, name, ldesc)) {
          if (remove)
            _trace_flags_ &= ~mask;
          else
            _trace_flags_ |= mask;
          break;
        } else if (name[lname] == 0)
          break;
        mask <<= 1;
        name += lname + 1;
      }
      desc += ldesc;
      break;
    }
  }
}
#else
#define _traces_(mask) (0)
#define _trace_file_   NULL
void ecoap_trace(FILE *file, const char *desc){}
#endif

/********************* SECTION: memory handling **************************/

#if ECOAP_DEBUG_MEMORY
static uint64_t _mem_alloc_ = 0;
static uint64_t _mem_freed_ = 0;
BARRIER(mem)

struct mem {
  uint64_t magic;
  size_t size;
};

#define _MEM_MAGIC_ 0x123456789abcdef0

static struct mem *_mem_(void *buffer)
{
  struct mem *mem = buffer;
  mem--;
  assert (mem->magic == _MEM_MAGIC_);
  return mem;
}

static void _mem_dump_(const char *txt, ssize_t sz)
{
  if (_traces_(trace_memory))
    printf("\nmemory: %s(%lld). a=%llu f=%llu d=%llu\n", txt, (long long int)sz, (long long unsigned)_mem_alloc_, (long long unsigned)_mem_freed_, (long long unsigned)(_mem_alloc_ - _mem_freed_));
}

static void *_mem_malloc_(size_t size)
{
  struct mem *mem = malloc(size + sizeof * mem);
  if (mem == NULL)
    return NULL;
  mem->magic = _MEM_MAGIC_;
  mem->size = size;
  LOCK(mem)
  _mem_alloc_ += size;
  _mem_dump_("malloc", size);
  UNLOCK(mem)
  return mem+1;
}

static void *_mem_realloc_(void *buffer, size_t size)
{
  struct mem *mem = _mem_(buffer);
  size_t osz = mem->size;
  mem = realloc(mem, size + sizeof * mem);
  if (mem == NULL)
    return NULL;
  mem->size = size;
  LOCK(mem)
  _mem_alloc_ += size;
  _mem_alloc_ -= osz;
  _mem_dump_("realloc", size-osz);
  UNLOCK(mem)
  return mem+1;
}

static void _mem_free_(void *buffer)
{
  struct mem *mem = _mem_(buffer);
  LOCK(mem)
  _mem_freed_ += mem->size;
  _mem_dump_("free",-mem->size);
  UNLOCK(mem)
  free(mem);
}
#else
# define _mem_malloc_  malloc
# define _mem_realloc_ realloc
# define _mem_free_    free
#endif

void (*ecoap_out_of_memory)() = abort;

static void *_malloc_(size_t size)
{
  void *result = _mem_malloc_(size);
  if (result == NULL)
    ecoap_out_of_memory();
  return result;
}

static void *_realloc_(void *buffer, size_t size)
{
  void *result = _mem_realloc_(buffer, size);
  if (result == NULL)
    ecoap_out_of_memory();
  return result;
}

static void _free_(void *buffer)
{
  _mem_free_(buffer);
}

/********************* SECTION: hazard ***********************************/

static uint16_t _hazard_value_ = 0;
BARRIER(hazard_byte)

static uint8_t _hazard_get_byte_()
{
  uint16_t v;
  LOCK(hazard_byte)
  v = _hazard_value_ * 27621 + 6897;
  _hazard_value_ = v;
  UNLOCK(hazard_byte)
  return (uint8_t)(v >> 8);
}

static void _hazard_init_()
{
  _hazard_value_ = (uint16_t)time(NULL);
}

#if ECOAP_SALT_TOKEN || ECOAP_SALT_MESSAGE_ID
static uint16_t _hazard_bits_ = 0;
BARRIER(hazard_bit)

static uint8_t _hazard_get_bit_()
{
  uint16_t v;
  LOCK(hazard_bit)
  v = _hazard_bits_;
  if (v <= 1)
    v = 256 | ((uint16_t)_hazard_get_byte_());
  _hazard_bits_ = v >> 1;
  UNLOCK(hazard_bit)
  return (uint8_t)(v & 1);
}
#endif

/********************* SECTION: token generation *************************/

static uint8_t _token_[GENERATED_TOKEN_LENGTH]; /* records some data for the token */
BARRIER(token)

/* creates a new token in _token_ and returns its length */
static uint8_t _token_next_()
{
  uint8_t i, n, b, a;

  /* increase the token value */
  a = 1;
#if ECOAP_SALT_TOKEN
  a += _hazard_get_bit_();
#endif
  n = i = 0;
  LOCK(token)
  do {
    b = _token_[i];
    a = a + b;
    _token_[i++] = a;
    if (a != 0)
      n = i;
    a = a < b;
  } while(i < sizeof _token_ / sizeof * _token_);

  /* restarting?! */
  if (n == 0) {
    _token_[0] = 1;
    n = 1;
  }
  UNLOCK(token)
  
  /* returns the length */
  return n;
}

/********************* SECTION: message id generation ********************/

static uint16_t _message_id_ = 0; /* records some data for token id */
BARRIER(message_id)

static void _message_id_next_()
{
  LOCK(message_id)
  do {
    _message_id_++;
#if ECOAP_SALT_MESSAGE_ID
    _message_id_ += _hazard_get_bit_();
#endif
  } while(_message_id_ == 0);
  UNLOCK(message_id)
}

/********************* SECTION: addresses ********************************/

/* address struct ensures that sockaddr fits the requirement */
struct address {
  socklen_t             length;     /* length of the address */
  union {
    struct sockaddr     addr_any;   /* any addresses */
    struct sockaddr_in  addr_ipv4;  /* IPv4 adresses */
    struct sockaddr_in6 addr_ipv6;  /* IPv6 adresses */
  };
};

#define MAX_ADDRESS_LENGTH  (sizeof(struct address)-(size_t)(&(((struct address*)0)->addr_any)))

/* compare 'first' and 'second' address return 0 if equals, <0 if first<second, >0 otherwise */
static int _address_compare_(const struct address *first, const struct address *second)
{
  int d = (int)(second->length - first->length);
  return d ? d : memcmp(first, second, second->length);
}

/* set the 'address' to be equal to 'sockaddr' of 'length' */
static void _address_set_(struct address *address, const struct sockaddr *sockaddr, socklen_t length)
{
  assert(length <= MAX_ADDRESS_LENGTH);
  memcpy(&address->addr_any, sockaddr, length);
  address->length = length;
}

/********************* SECTION: timespec *********************************/

/* set the 'timespec' to the time of now */
static void _timespec_now_(struct timespec *timespec)
{
  ifassert(int sts =) clock_gettime(CLOCK_MONOTONIC, timespec);
  assert(sts == 0);
}

/* is 'left' timespec lesser (older) or equal to 'right' time spec? */
static bool _timespec_leq_(const struct timespec *left, const struct timespec *right)
{
  return left->tv_sec < right->tv_sec || (left->tv_sec == right->tv_sec && left->tv_nsec <= right->tv_nsec);
}

/********************* SECTION: timeout **********************************/

/* get an initial retransmit timeout in 1/256° sec. */
static uint32_t _timeout_initial_retransmit_()
{
#if eCoAP_ACK_TIMEOUT_MIN_256 == 512 && eCoAP_ACK_TIMEOUT_MAX_256 == 768
  return ((uint32_t)_hazard_get_byte_()) + 512;
#else
  return eCoAP_ACK_TIMEOUT_MIN_256 + (((uint32_t)(eCoAP_ACK_TIMEOUT_MAX_256 - eCoAP_ACK_TIMEOUT_MIN_256) * _hazard_get_byte_()) >> 8);
#endif
}

/* Adds the 'timeout' in 1/256° sec to 'timespec'. */
static void _timeout_add_(uint32_t timeout, struct timespec *timespec)
{
  struct timespec t;

  /* time out to time spec */
  t.tv_sec = (time_t)(timeout >> 8);
  t.tv_nsec = (long)(timeout & 255);
#if ECOAP_TIME_APPROX == 0
  t.tv_nsec *= 3906250;
#elif ECOAP_TIME_APPROX == 1
  /* approximate the factor 10^9/256 by 238*16384. error = -0.18% */
  t.tv_nsec *= 238;
  t.tv_nsec <<= 14;
#elif ECOAP_TIME_APPROX == 2
  /* as above with 238=256-16-2 */
  t.tv_nsec = (t.tv_nsec << 22) - (t.tv_nsec << 18) - (t.tv_nsec << 15);
#else
# error "unsupported value for ECOAP_TIME_APPROX"
#endif

  /* add origin */
  t.tv_sec += timespec->tv_sec;
  t.tv_nsec += timespec->tv_nsec;
  if (t.tv_nsec > 1000000000) {
    t.tv_nsec -= 1000000000;
    t.tv_sec++;
  }

  /* store */
  *timespec = t;
}

/********************* SECTION: retransmit *******************************/

/* for handling retransmit times */
struct retransmit {
#if ECOAP_COMPRESS_RETRANSMIT
  unsigned count: 8,      /* retransmit count */
           timeout: 24;   /* unit is 1/256 second */
#else
  uint8_t count;          /* retransmit count */
  uint32_t timeout;       /* unit is 1/256 second */
#endif
  struct timespec expire; /* time of timeout expiration */
};

/* initialize the 'retransmit' structure for confirmable */
static void _retransmit_init_con_(struct retransmit *retransmit)
{
  retransmit->count = eCoAP_MAX_RETRANSMIT;
  _timespec_now_(&retransmit->expire);
  retransmit->timeout = _timeout_initial_retransmit_();
  _timeout_add_(retransmit->timeout, &retransmit->expire);
}

/* initialize the 'retransmit' structure for non confirmable */
static void _retransmit_init_non_(struct retransmit *retransmit)
{
  retransmit->count = 0;
  _timespec_now_(&retransmit->expire);
  _timeout_add_(eCoAP_MAX_TRANSMIT_WAIT_256, &retransmit->expire);
}

/* Checks if the maximum count of retransmission is reached for 'retransmit'.
If the maximum count of retransmit is not reached, the new timeout is computed.
Returns true if reached, false otherwise (should try to retransmit with new timeout) */
static bool _retransmit_next_(struct retransmit *retransmit)
{
  if (retransmit->count == 0)
    return false;
  retransmit->count--;
  retransmit->timeout <<= 1;
  _timeout_add_(retransmit->timeout, &retransmit->expire);
  return true;
}

/********************* SECTION: options **********************************/

#define _option_is_critical_(opt)  (((opt) & eCoAP_OPTION_CRITICAL_MASK) != 0)
#define _option_is_unsafe_(opt)    (((opt) & eCoAP_OPTION_UNSAFE_MASK) != 0)
#define _option_is_no_cache_(opt)  (((opt) & eCoAP_OPTION_NO_CACHE_MASK) == eCoAP_OPTION_NO_CACHE_VALUE)

/* recording the predefined options CAUTION!! sorted by id */
static const struct ecoap_option _option_predefineds_[] =
{
  { "If-Match"      , eCoAP_OPTION_If_Match      , true , eCoAP_OPTIONTYPE_OPAQUE, 0, 8    },
  { "Uri-Host"      , eCoAP_OPTION_Uri_Host      , false, eCoAP_OPTIONTYPE_STRING, 1, 255  },
  { "ETag"          , eCoAP_OPTION_ETag          , true , eCoAP_OPTIONTYPE_OPAQUE, 1, 8    },
  { "If-None-Match" , eCoAP_OPTION_If_None_Match , false, eCoAP_OPTIONTYPE_EMPTY , 0, 0    },
  { "Uri-Port"      , eCoAP_OPTION_Uri_Port      , false, eCoAP_OPTIONTYPE_UINT32, 0, 2    },
  { "Location-Path" , eCoAP_OPTION_Location_Path , true , eCoAP_OPTIONTYPE_STRING, 0, 255  },
  { "Uri-Path"      , eCoAP_OPTION_Uri_Path      , true , eCoAP_OPTIONTYPE_STRING, 0, 255  },
  { "Content-Format", eCoAP_OPTION_Content_Format, false, eCoAP_OPTIONTYPE_UINT32, 0, 2    },
  { "Max-Age"       , eCoAP_OPTION_Max_Age       , false, eCoAP_OPTIONTYPE_UINT32, 0, 4    },
  { "Uri-Query"     , eCoAP_OPTION_Uri_Query     , true , eCoAP_OPTIONTYPE_STRING, 0, 255  },
  { "Accept"        , eCoAP_OPTION_Accept        , false, eCoAP_OPTIONTYPE_UINT32, 0, 2    },
  { "Location-Query", eCoAP_OPTION_Location_Query, true , eCoAP_OPTIONTYPE_STRING, 0, 255  },
  { "Proxy-Uri"     , eCoAP_OPTION_Proxy_Uri     , false, eCoAP_OPTIONTYPE_STRING, 1, 1034 },
  { "Proxy-Scheme"  , eCoAP_OPTION_Proxy_Scheme  , false, eCoAP_OPTIONTYPE_STRING, 1, 255  },
  { "Size1"         , eCoAP_OPTION_Size1         , false, eCoAP_OPTIONTYPE_UINT32, 0, 4    }
};

/*
   Searchs in the array _option_predefineds_ the option
   of id 'option' and return its descriptor if found or
   NULL otherwise.
*/
const struct ecoap_option *ecoap_option_by_number(uint16_t option)
{
  int low = 0, mid, up = sizeof _option_predefineds_ / sizeof * _option_predefineds_;
  const struct ecoap_option *desc;

  /* dichotomic search */
  while (low < up) {
    mid = (low + up) >> 1;
    desc = &_option_predefineds_[mid];
    if (desc->id == option)
      return desc;
    if (desc->id < option)
      low = mid + 1;
    else
      up = mid;
  }
  return NULL;
}

/*
   Searchs in the array _option_predefineds_ the option
   of 'name' and return its descriptor if found or
   NULL otherwise.
*/
const struct ecoap_option *ecoap_option_by_name(const char *name)
{
  int n = sizeof _option_predefineds_ / sizeof * _option_predefineds_;
  const struct ecoap_option *desc = _option_predefineds_;
  uint16_t len = 1 + _strlen_(name);

  assert(n == sizeof _option_predefineds_ / sizeof * _option_predefineds_);

  while (n) {
    if (0 == memcmp(desc->name, name, len))
      return desc;
    n--;
    desc++;
  }
  return NULL;
}

/********************* SECTION: decoding options *************************/

/* decoded option data */
struct decopt {
  int16_t validity;  /* 0: end of options, 1: option, -1 or -2: error */
  uint16_t delta;    /* delta of option number */
  uint16_t length;   /* length of the option data */
  uint16_t offset;   /* ofsset of either the option or the content */
};

/* decode the option of the message of 'body' starting at 'offset' */
static struct decopt _decode_option_(const uint8_t *body, uint16_t offset)
{
  struct decopt result;
  uint8_t first;

  first = body[offset++];
  result.validity = (int16_t)!eCoAP_OPTION_IS_END(first);
  if (result.validity) {
    /* first: the delta option */
    result.delta = (first >> eCoAP_OPTION_DELTA_SHIFT) & eCoAP_OPTION_DELTA_MASK;
    if (result.delta >= eCoAP_OPTION_EXT1_VALUE) {
      if (result.delta == eCoAP_OPTION_EXT1_VALUE) {
        result.delta = body[offset++];
        result.delta += eCoAP_OPTION_EXT1_OFFSET;
      } else if (result.delta == eCoAP_OPTION_EXT2_VALUE) {
        result.delta = body[offset++];
        result.delta <<= 8;
        result.delta += body[offset++];
        result.delta += eCoAP_OPTION_EXT2_OFFSET;
      } else
        result.validity = -1;
    }
    /* second: the value length */
    result.length = (first >> eCoAP_OPTION_LENGTH_SHIFT) & eCoAP_OPTION_LENGTH_MASK;
    if (result.length >= eCoAP_OPTION_EXT1_VALUE) {
      if (result.length == eCoAP_OPTION_EXT1_VALUE) {
        result.length = body[offset++];
        result.length += eCoAP_OPTION_EXT1_OFFSET;
      } else if (result.length == eCoAP_OPTION_EXT2_VALUE) {
        result.length = body[offset++];
        result.length <<= 8;
        result.length += body[offset++];
        result.length += eCoAP_OPTION_EXT2_OFFSET;
      } else
        result.validity = -2;
    }
  }
  result.offset = offset;
  return result;
}

/********************* SECTION: message **********************************/

static const char *_type_2_name_[] = { "Confirmable", "Non-Confirmable", "Acknowledgement", "Reset" };

static const char *_class_2_name_[] = { "Request", "?", "Response", "?", "Client-Error", "Server-Error", "?", "?" };


/* structure for messages */
struct message {
  uint16_t length;  /* length of the message */
  uint8_t body[eCoAP_MINIMAL_MESSAGE_LENGTH];   /* data (body) of the message */
};

#define _message_version_(msg)      (((msg)->body[eCoAP_VERSION_INDEX] >> eCoAP_VERSION_SHIFT) & eCoAP_VERSION_MASK)
#define _message_type_(msg)         (((msg)->body[eCoAP_TYPE_INDEX] >> eCoAP_TYPE_SHIFT) & eCoAP_TYPE_MASK)
#define _message_code_(msg)         ((msg)->body[eCoAP_CODE_INDEX])
#define _message_class_(msg)        (((msg)->body[eCoAP_CODE_INDEX] >> eCoAP_CODE_CLASS_SHIFT) & eCoAP_CODE_CLASS_MASK)
#define _message_detail_(msg)       (((msg)->body[eCoAP_CODE_INDEX] >> eCoAP_CODE_DETAIL_SHIFT) & eCoAP_CODE_DETAIL_MASK)
#define _message_id_(msg)           ((((uint16_t)(msg)->body[eCoAP_MESSAGE_ID_INDEX_HIGH])<<8)|((uint16_t)(msg)->body[eCoAP_MESSAGE_ID_INDEX_LOW]))
#define _message_token_len_(msg)    (((msg)->body[eCoAP_TOKEN_LENGTH_INDEX] >> eCoAP_TOKEN_LENGTH_SHIFT) & eCoAP_TOKEN_LENGTH_MASK)
#define _message_token_(msg)        (&((msg)->body[eCoAP_TOKEN_INDEX]))
#define _message_token_at_(msg,idx) (_message_tok_(msg)[idx])

/* binary dump 'buffer' of 'length' to 'file' using 'prefix' */
static void _message_dump_aux_(FILE *file, const uint8_t *buffer, uint16_t length, const char *prefix)
{
  const uint16_t n = 8; /* width of a sub block */
  uint16_t i;

  while (length) {
    /* the prefix */
    fprintf(file, "%s         ", prefix);
    /* the sub block of width as hexa */
    for (i = 0 ; i < n ; i++)
       if (i < length)
         fprintf(file, " %02x", (int)buffer[i]);
       else
         fprintf(file, "   ");
    /* the sub block of width as chars */
    fprintf(file, "  ");
    for (i = 0 ; i < n ; i++)
       if (i < length)
         fprintf(file, "%c", (char)(32 <= buffer[i] && buffer[i] < 127 ? buffer[i] : '.'));
    fprintf(file, "\n");
    /* next sub block */
    if (length <= n)
      break;
    buffer += n;
    length -= n;
  }
}

/* dumps the message 'msg' to 'file' using 'prefix' */
static void _message_dump_(const struct message *msg, FILE *file, const char *prefix)
{
  uint16_t lastopt, i, offset;
  const struct ecoap_option *desc;
  struct decopt decopt;

  /* a minimum of being: enough length and good version */
  assert(msg->length >= eCoAP_MINIMAL_MESSAGE_LENGTH);

  if (prefix == NULL)
    prefix = "";

  fprintf(file, "\n%s---begin---\n", prefix);
  fprintf(file, "%s message  %d bytes\n", prefix, (int)msg->length);
  _message_dump_aux_(file, msg->body, msg->length, prefix);

  if (msg->length < eCoAP_MINIMAL_MESSAGE_LENGTH)
    return;

  fprintf(file, "%s version  %d\n", prefix, (int)_message_version_(msg));
  fprintf(file, "%s type     %d (%s)\n", prefix, (int)_message_type_(msg), _type_2_name_[_message_type_(msg)]);
  fprintf(file, "%s code     %d.%d (class %s)\n", prefix, (int)_message_class_(msg), (int)_message_detail_(msg), _class_2_name_[_message_class_(msg)]);
  fprintf(file, "%s id       %d\n", prefix, (int)_message_id_(msg));

  fprintf(file, "%s token   ", prefix);
  offset = eCoAP_TOKEN_INDEX;
  i = _message_token_len_(msg);
  while(i--)
    fprintf(file, " %02x", (int)msg->body[offset++]);
  fprintf(file, "\n");

  lastopt = eCoAP_OPTION_INITIAL;
  while (offset < msg->length) {
    decopt = _decode_option_(msg->body, offset);
    if (decopt.validity > 0) {
      lastopt += decopt.delta;
      desc = ecoap_option_by_number(lastopt);
      fprintf(file, "%s option   %d", prefix, lastopt);
      if (desc != NULL)
        fprintf(file, " = %s", desc->name);
      fprintf(file, "\n");
      _message_dump_aux_(file, msg->body + decopt.offset, decopt.length, prefix);
      offset = decopt.offset + decopt.length;
    } else if (decopt.validity == 0) {
      fprintf(file, "%s content\n", prefix);
      _message_dump_aux_(file, msg->body + decopt.offset, msg->length - decopt.offset, prefix);
      break;
    } else {
      fprintf(file, "%s ERROR AT OFFSET %d\n", prefix, offset);
      break;
    }
  }

  fprintf(file, "%s--- end ---\n", prefix);
}

/* valids (return 0) the buffer */
static int _message_valid_(const struct message *msg)
{
  uint8_t toklen;
  uint16_t lastopt, offset;
  const struct ecoap_option *desc;
  struct decopt decopt;

  /* a minimum of being: enough length and good version */
  assert(msg->length >= eCoAP_MINIMAL_MESSAGE_LENGTH);
  assert(_message_version_(msg) == eCoAP_VERSION_1);

  /* validate the token */
  toklen = _message_token_len_(msg);
  if (toklen > eCoAP_TOKEN_LENGTH_MAXIMUM)
    return -1; /* bad token length */
  offset = eCoAP_TOKEN_INDEX + toklen;
  if (msg->length < offset)
    return -2; /* truncated data */

  /* validate the options */
  lastopt = eCoAP_OPTION_INITIAL;
  while (offset < msg->length) {
    /* decode the option */
    decopt = _decode_option_(msg->body, offset);
    if (decopt.validity == 0)
      break; /* normal content detected */
    if (decopt.validity < 0)
      return (int)decopt.validity - 2; /* -> -3 (delta) and -4 (legnth) */

    /* check the option */
    lastopt += decopt.delta;
    desc = ecoap_option_by_number(lastopt);
    if (desc != NULL) {
      if (decopt.delta == 0 && !desc->repeatable)
        return -5; /* bad repeatable */
      if (decopt.length < desc->mini || desc->maxi < decopt.length)
        return -6; /* bad option size */
    } else if (_option_is_critical_(lastopt)) {
      /* unknown critical option */
      return -7; /* TODO diagnostic? */
    }

    /* check that it doesn't overflow */
    if (decopt.offset > msg->length)
      return -8; /* option overflow */
    offset = decopt.offset + decopt.length;
    if (offset > msg->length)
      return -9; /* option data overflow */
  }

  return 0;
}

/********************* SECTION: main *************************************/

/* is the initialisation done? */
static uint32_t _init_done_ = 0;

/* main epoll file descriptor */
static int _main_epfd_ = -1;


/********************* SECTION: timer 1 **********************************/

/* file descriptor of the timer */
static int _timer1_fd_;

/* state of the timer */
static enum { _t_unset_, _t_set_, _t_stored_ } _timer1_state_ = _t_unset_;

/* the timer */
static struct itimerspec _timer1_ = { { 0, 0 }, { 0, 0 } };

/* erase the timer */
static void _timer1_clear_()
{
  _timer1_state_ = _t_unset_;
}

/* store the timer to the alarm filedescriptor */
static void _timer1_store_()
{
  ifassert(int sts =) timerfd_settime(_timer1_fd_, TFD_TIMER_ABSTIME, &_timer1_, NULL);
  assert(sts == 0);
  _timer1_state_ = _t_stored_;
}

/* add, recording the nearest, the time and store it if nearest */
static void _timer1_add_(const struct timespec *timespec)
{
  if (_timer1_state_ == _t_unset_ || !_timespec_leq_(timespec, &_timer1_.it_value)) {
    _timer1_.it_value = *timespec;
    _timer1_store_();
  }
}

/* add, recording the nearest, the timer without storing it */
static void _timer1_lazy_add_(const struct timespec *timespec)
{
  if (_timer1_state_ == _t_unset_ || !_timespec_leq_(timespec, &_timer1_.it_value)) {
    _timer1_.it_value = *timespec;
    _timer1_state_ = _t_set_;
  }
}

/* stoe the timer if layly updated */
static void _timer1_lazy_store_()
{
  if (_timer1_state_ == _t_set_)
    _timer1_store_();
}

/********************* SECTION: interfaces **********************************/

struct interface {
  struct interface *next;  /* link to the next interface */
  int sock;                /* socket file descriptor */
  struct address address;  /* address of the interface */
  uint8_t nstart;          /* currently started count */
};

/* head of the list of the interfaces */
static struct interface *_interfaces_ = NULL;
BARRIER(interfaces)

/* default interface for sending */
static struct interface *_default_origin_ = NULL;

/* get the interface of 'itfid' */
static struct interface *_interface_get_(int itfid)
{
  struct interface *result;
  LOCK(interfaces)
  result = _interfaces_;
  while(result != NULL && result->sock != itfid)
    result = result->next;
  UNLOCK(interfaces)
  return result;
}

/* add an interface for the address 'addr' of length 'addrlen' */
int ecoap_interface_add(const struct sockaddr *addr, socklen_t addrlen)
{
  int sock;
  struct interface *interface;
  struct epoll_event epe;

  assert(_init_done_);

  /* check length */
  if (addrlen > MAX_ADDRESS_LENGTH) {
    errno = ENAMETOOLONG;
    return -1;
  }

  /* create the socket */
  sock = socket(addr->sa_family, SOCK_DGRAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0);
  if (sock >= 0) {
    /* bind the socket */
    if (0 == bind(sock, addr, addrlen)) {
      /* allocate the interface data and init it */
      interface = _malloc_(sizeof * interface);
      interface->sock = sock;
      _address_set_(&interface->address, addr, addrlen);
      /* record the interface in the epoll lookup */
      epe.events = EPOLLIN;
      epe.data.ptr = interface;
      if (0 == epoll_ctl(_main_epfd_, EPOLL_CTL_ADD, sock, &epe)) {
        /* record the interface and terminate successfully */
        interface->nstart = 0;
        LOCK(interfaces)
        interface->next = _interfaces_;
        _interfaces_ = interface;
        if (_default_origin_ == NULL)
          _default_origin_ = interface;
        UNLOCK(interfaces)
        return sock;
      }
      _free_(interface);
    }
    close(sock);
  }
  return -1;
}

/* add a default interface local or 'global' and 'bound' to coap port or to an other port */
int ecoap_interface_add_default(bool global, bool bound)
{
  struct addrinfo hint, *ai;
  int sts;
  const char *node = global ? NULL : "localhost";
  const char *service = bound ? eCoAP_DEFAULT_COAP_SERVICE : "0";

  memset(&hint, 0, sizeof hint);
  hint.ai_flags      = AI_PASSIVE;
  hint.ai_family     = AF_UNSPEC;
  hint.ai_family     = AF_INET;        /* TODO not only IPV4 please */
  hint.ai_socktype   = SOCK_DGRAM;
  sts = getaddrinfo(node, service, &hint, &ai);
  if (sts != 0) {
    errno = 0; /* TODO: what errno? */
    return -1;
  }
  sts = ecoap_interface_add(ai->ai_addr, ai->ai_addrlen);
  freeaddrinfo(ai);
  return sts;
}

/* sends 'message' to 'target' address throught 'interface' interface */
static int _interface_send_(const struct interface *interface, const struct address *target, const struct message *message)
{
  ssize_t sts;
  if (_traces_(trace_sent))
    _message_dump_(message, _trace_file_, "sends ");
  sts = sendto(interface->sock, message->body, message->length, 0/*TODO*/, &target->addr_any, target->length);
  if (sts < 0)
    return -1;
  if (sts == message->length)
    return 0;
  errno = EMSGSIZE;
  return -1;
}

/* sends an empty message of 'type' and 'msgid' to 'target' address throught 'interface' interface */
static int _interface_send_empty_(const struct interface *interface, const struct address *target, uint16_t msgid, uint8_t type)
{
  struct message message;
  assert(type == (type & eCoAP_TYPE_MASK));
  message.length = eCoAP_MINIMAL_MESSAGE_LENGTH;
  message.body[0] = (eCoAP_VERSION_1 << eCoAP_VERSION_SHIFT) | (type << eCoAP_TYPE_SHIFT);
  message.body[eCoAP_CODE_INDEX] = eCoAP_CODE_EMPTY;
  message.body[eCoAP_MESSAGE_ID_INDEX_HIGH] = (uint8_t)((msgid >> 8) & 255);
  message.body[eCoAP_MESSAGE_ID_INDEX_LOW] = (uint8_t)(msgid & 255);

  return _interface_send_(interface, target, &message);
}

void ecoap_put_default_interface(int itfid)
{
  struct interface *interface = _interface_get_(itfid);
  assert(interface != NULL);
  _default_origin_ = interface;
}

/********************* SECTION: incoming and outgoing structures *********/

/* structure for options or content creation */
struct outgitem {
  struct outgitem *link; /* link to the next */
  uint16_t option;       /* option number */
  uint16_t length;       /* data length */
  uint8_t data[];        /* the data */
};

struct incoming;

struct outgoing {
  struct outgoing *next;    /* next outgoing */
  struct address target;    /* address of the target */
  struct interface *interface;    /* interface for outgoing */

  union {
    /* part used during creation stage */
    struct {
      uint8_t type;             /* type of the message */
      uint8_t code;             /* code of the message */
      struct outgitem *options; /* options in reverse order */
      struct outgitem *content; /* content in reverse order */
    };
    /* part used during send stage */
    struct {
      uint8_t flags;                /* flags of the outgoing */
      uint8_t signaled;             /* state of the signaling */
      struct retransmit retransmit; /* retransmiting data */
      struct message *message;      /* message awaiting */
    };
  };
  union {
    struct incoming *reply;    /* the reply for a request */
    struct incoming *request;  /* the request for a reply */
  };
  void (*handler)(enum ecoap_event,void*); 
  void *userdata;            /* the user data for a request */
};

#define _OUT_AWAITING_        1    /* waits some answer */
#define _OUT_EXPIRED_         2    /* expiration reached */
#define _OUT_RESET_           4    /* reset emitted as reply */
#define _OUT_CONFIRMED_       8    /* acknowledge emitted as reply */
#define _OUT_REPLIED_        16    /* reply emitted */
#define _OUT_REMOVABLE_      32    /* can remove from the memory */

struct incoming {
  struct incoming *next;       /* next incoming */
  struct interface *interface; /* incoming from interface */
  struct timespec expire;      /* time of timeout expiration */
  struct address origin;       /* address of the sender */
  union {
    struct outgoing *request;    /* the matching request */
    struct outgoing *reply;      /* the matching reply */
  };
  uint8_t flags;               /* the flags */
  uint16_t option;             /* current option number */
  uint16_t index;              /* current repeat count of the current option */
  uint16_t offset;             /* offset of the data (content or option) within message body */
  uint16_t length;             /* length of the data (content or option) */
  struct message message;      /* the message -- MUST REMAIN THE LAST FIELD! */
};

#define _IN_AT_OPTION_     1
#define _IN_AT_CONTENT_    2
#define _IN_CONFIRMED_     4
#define _IN_RESET_         8
#define _IN_REPLIED_       16
#define _IN_OVERFLOWN_     32
#define _IN_STASHED_       64
#define _IN_TREATED_       128

/********************* SECTION: receiving ********************************/

static struct incoming *_incames_ = NULL;
static struct incoming *_incames_new_ = NULL;
static struct incoming *_incames_tail_ = NULL;
BARRIER(incames)

static __thread struct incoming *_current_incoming_ = NULL;

static struct incoming *_incames_search_(uint16_t msgid, struct address *origin)
{
  struct incoming *result;
  LOCK(incames)
  result = _incames_;
  while (result != NULL && (_message_id_(&result->message) != msgid || 0 != _address_compare_(&result->origin, origin)))
    result = result->next;
  UNLOCK(incames)
  return result;
}

static void _incames_sync_(const struct timespec *when)
{
  struct incoming *iter, *prev, *next;

  LOCK(incames)
  iter = _incames_;
  prev = NULL;
  while (iter != NULL && iter != _incames_new_) {
    next = iter->next;
    if ((iter->flags & _IN_TREATED_) == 0 || _timespec_leq_(when, &iter->expire)) {
      _timer1_lazy_add_(&iter->expire);
      prev = iter;
    } else {
      if (iter == _incames_tail_)
        _incames_tail_ = prev;
      if (prev == NULL)
        _incames_ = next;
      else
        prev->next = next;
      _free_(iter);
    }
    iter = next;
  }
  UNLOCK(incames)
}

static int _income_ack_(const struct incoming *income)
{
  return _interface_send_empty_(income->interface, &income->origin, _message_id_(&income->message), eCoAP_TYPE_ACKNOWLEDGMENT);
}

static int _income_reset_(const struct incoming *income)
{
  return _interface_send_empty_(income->interface, &income->origin, _message_id_(&income->message), eCoAP_TYPE_RESET);
}

bool ecoap_is_reading()
{
  return _current_incoming_ != NULL;
}

void ecoap_confirm()
{
  assert(ecoap_is_reading());
  if (_message_type_(&_current_incoming_->message) == eCoAP_TYPE_CONFIRMABLE && !(_current_incoming_->flags & (_IN_CONFIRMED_|_IN_RESET_))) {
    _current_incoming_->flags |= _IN_CONFIRMED_;
    _income_ack_(_current_incoming_);
  }
}

void ecoap_reset()
{
  assert(ecoap_is_reading());
  assert(!(_current_incoming_->flags & _IN_RESET_));
  _current_incoming_->flags |= _IN_RESET_;
  _income_reset_(_current_incoming_);
  /* note: resetting _IN_CONFIRMED_ here is not needed because _IN_RESET_ has higher priority. */
}

void *ecoap_stash()
{
  struct incoming *in = _current_incoming_;
  assert(in != NULL);
  _current_incoming_ = NULL;
  in->flags |= _IN_STASHED_;
  return in;
}

void ecoap_unstash(void *stash)
{
  struct incoming *in = stash;
  assert(_current_incoming_ == NULL);
  assert(in != NULL);
  assert(in->flags & _IN_STASHED_);
  in->flags &= ~_IN_STASHED_;
  _current_incoming_ = in;
}

static void _income_treated_(struct incoming *in)
{
  in->flags |= _IN_TREATED_;
  /* TODO check if confirm is needed */
  if (in->request != NULL)
    in->request->flags = _OUT_REMOVABLE_;
}

void ecoap_treated()
{
  struct incoming *in = _current_incoming_;
  _current_incoming_ = NULL;
  assert(in != NULL);
  _income_treated_(in);
}

void ecoap_forget()
{
  struct incoming *iter, *prev, *in = _current_incoming_;
  _current_incoming_ = NULL;
  assert(in != NULL);
  _income_treated_(in);
  LOCK(incames)
  iter = _incames_;
  prev = NULL;
  while (iter != in) {
    assert(iter != NULL);
    assert(iter != _incames_new_);
    prev = iter;
    iter = iter->next;
  }
  if (iter == _incames_tail_)
    _incames_tail_ = prev;
  if (prev == NULL)
    _incames_ = in->next;
  else
    prev->next = in->next;
  _free_(in);
  UNLOCK(incames)
}

int ecoap_get_interface()
{
  assert(_current_incoming_ != NULL);
  return _current_incoming_->interface->sock;
}

socklen_t ecoap_get_destination(const struct sockaddr **addr)
{
  assert(_current_incoming_ != NULL);
  assert(addr != NULL);
  *addr = &_current_incoming_->origin.addr_any;
  return _current_incoming_->origin.length;
}

uint8_t ecoap_get_code()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message);
}

uint8_t ecoap_get_class()
{
  assert(_current_incoming_ != NULL);
  return _message_class_(&_current_incoming_->message);
}

uint8_t ecoap_get_detail()
{
  assert(_current_incoming_ != NULL);
  return _message_detail_(&_current_incoming_->message);
}

bool ecoap_is_request()
{
  assert(_current_incoming_ != NULL);
  return _message_class_(&_current_incoming_->message) == eCoAP_CODE_CLASS_REQUEST;
}

bool ecoap_is_response()
{
  assert(_current_incoming_ != NULL);
  return _message_class_(&_current_incoming_->message) == eCoAP_CODE_CLASS_RESPONSE;
}

bool ecoap_is_error()
{
  assert(_current_incoming_ != NULL);
  return _message_class_(&_current_incoming_->message) == eCoAP_CODE_CLASS_CLIENT_ERROR || _message_class_(&_current_incoming_->message) == eCoAP_CODE_CLASS_SERVER_ERROR;
}

bool ecoap_is_client_error()
{
  assert(_current_incoming_ != NULL);
  return _message_class_(&_current_incoming_->message) == eCoAP_CODE_CLASS_CLIENT_ERROR;
}

bool ecoap_is_server_error()
{
  assert(_current_incoming_ != NULL);
  return _message_class_(&_current_incoming_->message) == eCoAP_CODE_CLASS_SERVER_ERROR;
}

bool ecoap_is_get()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_GET;
}

bool ecoap_is_post()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_POST;
}

bool ecoap_is_put()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_PUT;
}

bool ecoap_is_delete()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_DELETE;
}

bool ecoap_is_created()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_Created;
}

bool ecoap_is_deleted()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_Deleted;
}

bool ecoap_is_valid()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_Valid;
}

bool ecoap_is_changed()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_Changed;
}

bool ecoap_is_content()
{
  assert(_current_incoming_ != NULL);
  return _message_code_(&_current_incoming_->message) == eCoAP_CODE_Content;
}

bool ecoap_at_option()
{
  assert(_current_incoming_ != NULL);
  return (_current_incoming_->flags & _IN_AT_OPTION_) != 0;
}

bool ecoap_at_content()
{
  assert(_current_incoming_ != NULL);
  return (_current_incoming_->flags & _IN_AT_CONTENT_) != 0;
}

bool ecoap_at_data()
{
  assert(_current_incoming_ != NULL);
  return (_current_incoming_->flags & (_IN_AT_OPTION_|_IN_AT_CONTENT_)) != 0;
}

uint16_t ecoap_option_number()
{
  assert(ecoap_at_option());
  return _current_incoming_->option;
}

uint16_t ecoap_option_index()
{
  assert(ecoap_at_option());
  return _current_incoming_->index;
}

void _rewind_(struct incoming *income)
{
  struct decopt decopt;
  uint16_t offset;

  offset = eCoAP_TOKEN_INDEX + _message_token_len_(&income->message);
  if (offset == income->message.length) {
    /* no option, no content ! why not. */
    income->flags &= ~(_IN_AT_OPTION_|_IN_AT_CONTENT_);
  } else {
    assert(offset < income->message.length);
    decopt = _decode_option_(income->message.body, offset);
    assert(decopt.validity >= 0);
    if (decopt.validity == 0) {
      /* no option, select content */
      income->offset = decopt.offset;
      income->length = income->message.length - decopt.offset;
      income->flags = (income->flags & ~_IN_AT_OPTION_) | _IN_AT_CONTENT_;
    } else {
      /* first option */
      assert(decopt.delta > 0); /* This assertion allow to use eCoAP_OPTION_INITIAL as content indicator */
      income->option = eCoAP_OPTION_INITIAL + decopt.delta;
      income->index = 0;
      income->offset = decopt.offset;
      income->length = decopt.length;
      income->flags = (income->flags & ~_IN_AT_CONTENT_) | _IN_AT_OPTION_;
    }
  }
}

void ecoap_rewind()
{
  assert(ecoap_is_reading());
  _rewind_(_current_incoming_);
}

void ecoap_next()
{
  struct decopt decopt;
  uint16_t offset;

  assert(ecoap_at_option());

  offset = _current_incoming_->offset + _current_incoming_->length;
  if (offset == _current_incoming_->message.length) {
    /* no more option without content */
    _current_incoming_->flags &= ~(_IN_AT_OPTION_|_IN_AT_CONTENT_);
  } else {
    assert(offset < _current_incoming_->message.length);
    decopt = _decode_option_(_current_incoming_->message.body, offset);
    if (decopt.validity == 0) {
      /* no more option with content */
      _current_incoming_->offset = decopt.offset;
      _current_incoming_->length = _current_incoming_->message.length - decopt.offset;
      _current_incoming_->flags = (_current_incoming_->flags & ~_IN_AT_OPTION_) | _IN_AT_CONTENT_;
    } else {
      assert(decopt.validity > 0);
      /* next option */
      if (decopt.delta == 0) {
        _current_incoming_->index++;
        assert(_current_incoming_->index != 0); /* no overflow */
      } else {
        assert(_current_incoming_->option < _current_incoming_->option + decopt.delta); /* no overflow */
        _current_incoming_->option += decopt.delta;
        _current_incoming_->index = 0;
      }
      _current_incoming_->offset = decopt.offset;
     _current_incoming_->length = decopt.length;
    }
  }
}

bool ecoap_select_option_at(uint16_t option, uint16_t index)
{
  struct decopt decopt;
  uint16_t offset, opt, idx;
  bool rewind;

  assert(ecoap_is_reading());

  if (!(_current_incoming_->flags & _IN_AT_OPTION_))
    rewind = true;
  else if (option < _current_incoming_->option)
    rewind = true;
  else if (option == _current_incoming_->option) {
    if (index == _current_incoming_->index)
      return true;
    rewind = index > _current_incoming_->index;
  } else
    rewind = false;

  if (rewind) {
    offset = eCoAP_TOKEN_INDEX + _message_token_len_(&_current_incoming_->message);
    opt = eCoAP_OPTION_INITIAL;
    idx = 0;
  } else {
    offset = _current_incoming_->offset + _current_incoming_->length;
    opt = _current_incoming_->option;
    idx = _current_incoming_->index;
  }

  for (;;) {
    assert(opt <= option);
    assert(idx < index || opt < option);
    /* check existing option */
    if (offset >= _current_incoming_->message.length)
      return false;
    /* decode the option */
    decopt = _decode_option_(_current_incoming_->message.body, offset);
    assert(decopt.validity >= 0);
    /* is start of content? */
    if (decopt.validity == 0)
      return false;
    /* next option */
    if (decopt.delta == 0) {
      idx++;
      assert(idx != 0); /* no overflow */
    } else {
      assert(opt < opt + decopt.delta); /* no overflow */
      opt += decopt.delta;
      if (opt > option)
        return false;
      idx = 0;
    }
    /* is found? */
    if (opt == option) {
      assert(idx <= index);
      if (idx == index) {
        _current_incoming_->option = opt;
        _current_incoming_->index = idx;
        _current_incoming_->offset = decopt.offset;
        _current_incoming_->length = decopt.length;
        _current_incoming_->flags = (_current_incoming_->flags & ~_IN_AT_CONTENT_) | _IN_AT_OPTION_;
        return true;
      }
    }
    /* iteration */
    offset = decopt.offset + decopt.length;
  }
}

bool ecoap_select_option(uint16_t option)
{
  return ecoap_select_option_at(option, 0);
}

bool ecoap_select_next_sibling()
{
  struct decopt decopt;
  uint16_t offset;

  assert(ecoap_at_option());

  offset = _current_incoming_->offset + _current_incoming_->length;

  /* check existing option */
  if (offset >= _current_incoming_->message.length)
    return false;

  /* decode the option */
  decopt = _decode_option_(_current_incoming_->message.body, offset);
  assert(decopt.validity >= 0);

  /* is start of content? */
  if (decopt.validity == 0 || decopt.delta != 0)
    return false;

  _current_incoming_->index++;
  _current_incoming_->offset = decopt.offset;
  _current_incoming_->length = decopt.length;
  return true;
}

bool ecoap_select_content()
{
  struct decopt decopt;
  uint16_t offset;

  assert(ecoap_is_reading());

  if ((_current_incoming_->flags & _IN_AT_CONTENT_) != 0)
    return true;

  if ((_current_incoming_->flags & _IN_AT_OPTION_) != 0)
    offset = _current_incoming_->offset + _current_incoming_->length;
  else
    offset = eCoAP_TOKEN_INDEX + _message_token_len_(&_current_incoming_->message);

  for (;;) {
    /* check existing option */
    if (offset >= _current_incoming_->message.length) {
      /* at end without content */
      assert(offset == _current_incoming_->message.length);
      return false;
    }
    /* decode the option */
    decopt = _decode_option_(_current_incoming_->message.body, offset);
    assert(decopt.validity >= 0);
    /* is start of content? */
    offset = decopt.offset;
    if (decopt.validity == 0) {
      _current_incoming_->offset = offset;
      _current_incoming_->length = _current_incoming_->message.length - offset;
      _current_incoming_->flags = (_current_incoming_->flags & ~_IN_AT_OPTION_) | _IN_AT_CONTENT_;
      return true;
    }
    /* next option */
    offset += decopt.length;
  }
}

uint16_t ecoap_get_length()
{
  assert(ecoap_at_data());
  return _current_incoming_->length;
}

const void *ecoap_get_pointer()
{
  assert(ecoap_at_data());
  return _current_incoming_->message.body + _current_incoming_->offset;
}

struct ecoap_data ecoap_get_data()
{
  struct ecoap_data result;
  assert(ecoap_at_data());
  result.length = _current_incoming_->length;
  result.data = _current_incoming_->message.body + _current_incoming_->offset;
  return result;
}

uint32_t ecoap_get_uint32()
{
  const uint8_t *data;
  uint32_t x = 0;

  assert(ecoap_at_data());
  assert (_current_incoming_->length <= 4);

  data = _current_incoming_->message.body + _current_incoming_->offset;
  switch(_current_incoming_->length) {
  case 4:
    x = *data++;
    x <<= 8;
  case 3:
    x |= *data++;
    x <<= 8;
  case 2:
    x |= *data++;
    x <<= 8;
  case 1:
    x |= *data;
  }
  return x;
}

void *ecoap_get_copy()
{
  void *result;

  assert(ecoap_at_data());

  result = malloc(_current_incoming_->length);
  if (result != NULL)
    memcpy(result, _current_incoming_->message.body + _current_incoming_->offset, _current_incoming_->length);

  return result;
}

char *ecoap_get_copyz()
{
  char *result;

  assert(ecoap_at_data());

  result = malloc(_current_incoming_->length + 1);
  if (result != NULL) {
    memcpy(result, _current_incoming_->message.body + _current_incoming_->offset, _current_incoming_->length);
    result[_current_incoming_->length] = 0;
  }
  return result;
}

int ecoap_compare(const char *text,  uint16_t length)
{
  int result;
  struct ecoap_data d = ecoap_get_data();
  if (length == d.length)
    result = memcmp(d.data, text, length);
  else if (length < d.length) {
    result = memcmp(d.data, text, length);
    result += !result;
  } else {
    result = memcmp(d.data, text, d.length);
    result -= !result;
  }
  return result;
}

int ecoap_comparez(const char *text)
{
  struct ecoap_data d = ecoap_get_data();
  uint16_t index = 0;
  int t = (int)(uint8_t)text[0];
  while (index < d.length) {
    int delta = (int)d.data[index] - t;
    if (delta != 0 || t == 0)
      return delta;
    t = (int)(uint8_t)text[++index];
  }
  return -t;
}

bool ecoap_select_if_match()
{
  return ecoap_select_option(eCoAP_OPTION_If_Match);
}

bool ecoap_select_uri_host()
{
  return ecoap_select_option(eCoAP_OPTION_Uri_Host);
}

bool ecoap_select_e_tag()
{
  return ecoap_select_option(eCoAP_OPTION_ETag);
}

bool ecoap_select_if_none_match()
{
  return ecoap_select_option(eCoAP_OPTION_If_None_Match);
}

bool ecoap_select_uri_port()
{
  return ecoap_select_option(eCoAP_OPTION_Uri_Port);
}

bool ecoap_select_location_path()
{
  return ecoap_select_option(eCoAP_OPTION_Location_Path);
}

bool ecoap_select_uri_path()
{
  return ecoap_select_option(eCoAP_OPTION_Uri_Path);
}

bool ecoap_select_content_format()
{
  return ecoap_select_option(eCoAP_OPTION_Content_Format);
}

bool ecoap_select_max_age()
{
  return ecoap_select_option(eCoAP_OPTION_Max_Age);
}

bool ecoap_select_uri_query()
{
  return ecoap_select_option(eCoAP_OPTION_Uri_Query);
}

bool ecoap_select_accept()
{
  return ecoap_select_option(eCoAP_OPTION_Accept);
}

bool ecoap_select_location_query()
{
  return ecoap_select_option(eCoAP_OPTION_Location_Query);
}

bool ecoap_select_proxy_uri()
{
  return ecoap_select_option(eCoAP_OPTION_Proxy_Uri);
}

bool ecoap_select_proxy_scheme()
{
  return ecoap_select_option(eCoAP_OPTION_Proxy_Scheme);
}

bool ecoap_select_size1()
{
  return ecoap_select_option(eCoAP_OPTION_Size1);
}

/********************* SECTION: outgoings ********************************/

static __thread struct outgoing *_current_outgoing_ = NULL;

static void _outgoing_(uint8_t code, struct incoming *incoming)
{
  struct outgoing *outgoing = _malloc_(sizeof * _current_outgoing_);
  outgoing->type = eCoAP_TYPE_NON_CONFIRMABLE;
  outgoing->code = code;
  outgoing->next = NULL;
  outgoing->options = NULL;
  outgoing->content = NULL;
  outgoing->userdata = NULL;
  outgoing->handler = NULL;
  outgoing->request = incoming;
  if (incoming == NULL) {
    outgoing->target.length = 0;
    outgoing->interface = NULL;
  } else {
    outgoing->interface = incoming->interface;
    outgoing->target = incoming->origin;
  }
  assert(_current_outgoing_ == NULL);
  _current_outgoing_ = outgoing;
}

static struct outgitem *_outgitem_add_(struct outgitem **head, uint16_t length, uint16_t option)
{
  struct outgitem *result;

  result = _malloc_(length + sizeof * result);
  result->option = option;
  result->length = length;
  while ((*head) != NULL && (*head)->option > option)
    head = &((*head)->link);
  result->link = *head;
  *head = result;
  return result;
}

/* converts the _current_outgoing_ from its creation stage to its send stage */
static void _outgoing_message_(struct outgoing *outgoing)
{
  struct message *message;
  uint16_t len, length, iopt, popt, dopt, index, sidx, msgid;
  struct outgitem *item, *link, *optitem, *conitem;
  uint8_t toklen;
  const uint8_t *token;

  assert(outgoing != NULL);

  /* reverse the option list */
  length = 4;
  item = outgoing->options;
  optitem = NULL;
  while (item) {
    link = item->link;
    item->link = optitem;
    optitem = item;
    item = link;
    len = optitem->length;
    dopt = item == NULL ? optitem->option : optitem->option-item->option;
    length += len + 1 + (len > 12) + (len > 268) + (dopt > 12) + (dopt > 268);
  }

  /* reverse the content list */
  item = outgoing->content;
  conitem = NULL;
  if (item != NULL) {
    length++;
    do {
      length += item->length;
      link = item->link;
      item->link = conitem;
      conitem = item;
      item = link;
    } while (item != NULL);
  }

  /* token handling */
  if (outgoing->request == NULL) {
    toklen = _token_next_();
    token = _token_;
  } else {
    toklen = _message_token_len_(&outgoing->request->message);
    token = _message_token_(&outgoing->request->message);
  } 
  length += toklen;

  /* allocate the message and init its header */
  message = _malloc_(length + (sizeof * message - eCoAP_MINIMAL_MESSAGE_LENGTH));
  message->length = length;

  assert(eCoAP_VERSION_INDEX == 0 && eCoAP_TYPE_INDEX == 0 && eCoAP_TOKEN_LENGTH_INDEX == 0);
  assert(outgoing->type == (outgoing->type & eCoAP_TYPE_MASK));
  assert(toklen == (toklen & eCoAP_TOKEN_LENGTH_MASK));
  assert(outgoing->code == (outgoing->code & eCoAP_CODE_MASK));

  message->body[0] = (eCoAP_VERSION_1 << eCoAP_VERSION_SHIFT) | (outgoing->type << eCoAP_TYPE_SHIFT) | (toklen << eCoAP_TOKEN_LENGTH_SHIFT);
  message->body[eCoAP_CODE_INDEX] = outgoing->code;

  _message_id_next_();
  msgid = _message_id_;
  message->body[eCoAP_MESSAGE_ID_INDEX_HIGH] = (uint8_t)((msgid >> 8) & 255);
  message->body[eCoAP_MESSAGE_ID_INDEX_LOW] = (uint8_t)(msgid & 255);

  /* set the index */
  memcpy(&message->body[eCoAP_TOKEN_INDEX], token, toklen);
  index = eCoAP_TOKEN_INDEX + toklen;

  /* put the options */
  popt = eCoAP_OPTION_INITIAL;
  while (optitem != NULL) {
    iopt = optitem->option;
    length = optitem->length;
    dopt = iopt - popt;
    sidx = index++;
    if (dopt >= 13) {
      if (dopt < 269) {
        dopt -= 13;
        assert(dopt <= 255);
        message->body[index++] = (uint8_t)dopt;
        dopt = 13;
      } else {
        dopt -= 269;
        message->body[index++] = (uint8_t)(dopt >> 8);
        message->body[index++] = (uint8_t)(dopt & 255);
        dopt = 14;
      }
    }
    if (length < eCoAP_OPTION_EXT1_OFFSET)
      len = length;
    else if (length < eCoAP_OPTION_EXT2_OFFSET) {
      len = length - eCoAP_OPTION_EXT2_OFFSET;
      message->body[index++] = (uint8_t)len;
      len = eCoAP_OPTION_EXT1_VALUE;
    } else {
      len = length - eCoAP_OPTION_EXT2_OFFSET;
      message->body[index++] = (uint8_t)(len >> 8);
      message->body[index++] = (uint8_t)(len & 255);
      len = eCoAP_OPTION_EXT2_VALUE;
    }
    message->body[sidx] = (dopt << eCoAP_OPTION_DELTA_SHIFT) | (len << eCoAP_OPTION_LENGTH_SHIFT);
    memcpy(&message->body[index], optitem->data, length);
    index += length;
    popt = iopt;
    item = optitem;
    optitem = optitem->link;
    _free_(item);
  }

  /* put the content */
  if (conitem != NULL) {
    message->body[index++] = eCoAP_CONTENT_INIT;
    do {
      length = conitem->length;
      memcpy(&message->body[index], conitem->data, length);
      index += length;
      item = conitem;
      conitem = conitem->link;
      _free_(item);
    } while (conitem != NULL);
  }

  /* record the message */
  outgoing->message = message;
}

static int _outgoing_send_(const struct outgoing *outgoing)
{
  return _interface_send_(outgoing->interface, &outgoing->target, outgoing->message);
}

bool ecoap_is_writing()
{
  return _current_outgoing_ != NULL;
}

void ecoap_cancel()
{
  struct outgitem *oitem;
  struct outgitem *noitem;

  if (_current_outgoing_ != NULL) {
    oitem = _current_outgoing_->options;
    while (oitem != NULL) {
      noitem = oitem->link;
      _free_(oitem);
      oitem = noitem;
    }
    oitem = _current_outgoing_->content;
    while (oitem != NULL) {
      noitem = oitem->link;
      _free_(oitem);
      oitem = noitem;
    }
    _free_(_current_outgoing_);
    _current_outgoing_ = NULL;
  }
  assert(!ecoap_is_writing());
}

bool ecoap_has_put_option(uint16_t option)
{
  struct outgitem *oitem;

  assert(ecoap_is_writing());
  oitem = _current_outgoing_->options;
  while (oitem != NULL && oitem->option > option)
    oitem = oitem->link;
  return oitem != NULL && oitem->option == option;
}

void ecoap_put_interface(int itfid)
{
  struct interface *interface = _interface_get_(itfid);
  assert(ecoap_is_writing());
  assert(interface != NULL);
  _current_outgoing_->interface = interface;
}

void ecoap_put_destination(const struct sockaddr *addr, socklen_t addrlen)
{
  assert(ecoap_is_writing());
  _address_set_(&_current_outgoing_->target, addr, addrlen);
}

void ecoap_put_handler(void (*handler)(enum ecoap_event event, void *userdata))
{
  assert(ecoap_is_writing());
  _current_outgoing_->handler = handler;
}

void ecoap_put_userdata(void *userdata)
{
  assert(ecoap_is_writing());
  _current_outgoing_->userdata = userdata;
}

void ecoap_request(uint8_t code)
{
  assert(!ecoap_is_writing());
  assert(eCoAP_CODE_CLASS_REQUEST == ((code >> eCoAP_CODE_CLASS_SHIFT) & eCoAP_CODE_CLASS_MASK));
  _outgoing_(code, NULL);
  assert(ecoap_is_writing());
}

void ecoap_reply(uint8_t code)
{
  assert(ecoap_is_reading());
  assert(!ecoap_is_writing());
  assert(eCoAP_CODE_CLASS_REQUEST != ((code >> eCoAP_CODE_CLASS_SHIFT) & eCoAP_CODE_CLASS_MASK));
  _outgoing_(code, _current_incoming_);
  assert(ecoap_is_writing());
}

void ecoap_request_get()
{
  ecoap_request(eCoAP_CODE_GET);
}

void ecoap_request_post()
{
  ecoap_request(eCoAP_CODE_POST);
}

void ecoap_request_put()
{
  ecoap_request(eCoAP_CODE_PUT);
}

void ecoap_request_delete()
{
  ecoap_request(eCoAP_CODE_DELETE);
}

void ecoap_reply_created()
{
  ecoap_reply(eCoAP_CODE_Created);
}

void ecoap_reply_deleted()
{
  ecoap_reply(eCoAP_CODE_Deleted);
}

void ecoap_reply_valid()
{
  ecoap_reply(eCoAP_CODE_Valid);
}

void ecoap_reply_changed()
{
  ecoap_reply(eCoAP_CODE_Changed);
}

void ecoap_reply_content()
{
  ecoap_reply(eCoAP_CODE_Content);
}

void ecoap_reply_bad_request()
{
  ecoap_reply(eCoAP_CODE_Bad_Request);
}

void ecoap_reply_unauthorized()
{
  ecoap_reply(eCoAP_CODE_Unauthorized);
}

void ecoap_reply_bad_option()
{
  ecoap_reply(eCoAP_CODE_Bad_Option);
}

void ecoap_reply_forbidden()
{
  ecoap_reply(eCoAP_CODE_Forbidden);
}

void ecoap_reply_not_found()
{
  ecoap_reply(eCoAP_CODE_Not_Found);
}

void ecoap_reply_method_not_allowed()
{
  ecoap_reply(eCoAP_CODE_Method_Not_Allowed);
}

void ecoap_reply_not_acceptable()
{
  ecoap_reply(eCoAP_CODE_Not_Acceptable);
}

void ecoap_reply_precondition_failed()
{
  ecoap_reply(eCoAP_CODE_Precondition_Failed);
}

void ecoap_reply_request_entity_too_large()
{
  ecoap_reply(eCoAP_CODE_Request_Entity_Too_Large);
}

void ecoap_reply_unsupported_content_format()
{
  ecoap_reply(eCoAP_CODE_Unsupported_Content_Format);
}

void ecoap_reply_internal_server_error()
{
  ecoap_reply(eCoAP_CODE_Internal_Server_Error);
}

void ecoap_reply_not_implemented()
{
  ecoap_reply(eCoAP_CODE_Not_Implemented);
}

void ecoap_reply_bad_gateway()
{
  ecoap_reply(eCoAP_CODE_Bad_Gateway);
}

void ecoap_reply_service_unavailable()
{
  ecoap_reply(eCoAP_CODE_Service_Unavailable);
}

void ecoap_reply_gateway_timeout()
{
  ecoap_reply(eCoAP_CODE_Gateway_Timeout);
}

void ecoap_reply_proxying_not_supported()
{
  ecoap_reply(eCoAP_CODE_Proxying_Not_Supported);
}

void ecoap_put_confirmable(bool confirm)
{
  assert(ecoap_is_writing());
  _current_outgoing_->type = confirm ? eCoAP_TYPE_CONFIRMABLE : eCoAP_TYPE_NON_CONFIRMABLE;
}

void ecoap_put_option_opaque(const void *buffer, uint16_t length, uint16_t option)
{
  struct outgitem *item;

  assert(ecoap_is_writing());
  item = _outgitem_add_(&_current_outgoing_->options, length, option);
  memcpy(item->data, buffer, length);
}

void ecoap_put_option_string(const char *string, uint16_t length, uint16_t option)
{
  ecoap_put_option_opaque(string, length, option);
}

void ecoap_put_option_stringz(const char *string, uint16_t option)
{
  ecoap_put_option_string(string, _strlen_(string), option);
}

static uint16_t _decode_memcpy_(uint8_t *to, const char *from, uint16_t length)
{
  char a, b, c;
  uint16_t i = 0, j = 0;
  while (i < length) {
    a = from[i++];
    if (a == '%' && i + 2 <= length) {
      b = from[i];
      if (('0' <= b && b <= '9') || ((b&=~' '), ('A' <= b && b <= 'F'))) {
        c= from[i+1];
        if (('0' <= c && c <= '9') || ((c&=~' '), ('A' <= c && c <= 'F'))) {
          a = ((b - (b <= '9' ? '0' : '7')) << 4) | (c - (c <= '9' ? '0' : '7'));
          i += 2;
        }
      }
    }
    to[j++] = (uint8_t)a;
  }
  return j;
}

void ecoap_put_option_encoded(const char *string, uint16_t length, uint16_t option)
{
  struct outgitem *item;

  assert(ecoap_is_writing());
  item = _outgitem_add_(&_current_outgoing_->options, length, option);
  item->length = _decode_memcpy_(item->data, string, length);
}

void ecoap_put_option_encodedz(const char *string, uint16_t option)
{
  ecoap_put_option_encoded(string, _strlen_(string), option);
}

void ecoap_put_option_empty(uint16_t option)
{
  ecoap_put_option_opaque(NULL, 0, option);
}

void ecoap_put_option_uint32(uint32_t value, uint16_t option)
{
  uint8_t i;
  union { uint32_t i; uint8_t a[4]; } x;
  x.i = htonl(value);
  if (x.a[0] != 0)
    i = 0;
  else if (x.a[1] != 0)
    i = 1;
  else if (x.a[2] != 0)
    i = 2;
  else if (x.a[3] != 0)
    i = 3;
  else
    i = 4;
  ecoap_put_option_opaque(x.a+i, 4-i, option);
}

void ecoap_put_if_match(const void *value, uint16_t length)
{
  ecoap_put_option_opaque(value, length, eCoAP_OPTION_If_Match);
}

void ecoap_put_if_matchz(const char *value)
{
  ecoap_put_if_match(value, _strlen_(value));
}

void ecoap_put_uri_host(const char *value, uint16_t length)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Uri_Host));
  ecoap_put_option_encoded(value, length, eCoAP_OPTION_Uri_Host);
}

void ecoap_put_uri_hostz(const char *value)
{
  ecoap_put_uri_host(value, _strlen_(value));
}

void ecoap_put_e_tag(const void *value, uint16_t length)
{
  ecoap_put_option_opaque(value, length, eCoAP_OPTION_ETag);
}

void ecoap_put_e_tagz(const char *value)
{
  ecoap_put_e_tag(value, _strlen_(value));
}

void ecoap_put_if_none_match()
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_If_None_Match));
  ecoap_put_option_empty(eCoAP_OPTION_If_None_Match);
}

void ecoap_put_uri_port(uint16_t value)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Uri_Port));
  ecoap_put_option_uint32(value, eCoAP_OPTION_Uri_Port);
}

void ecoap_put_location_path(const char *value, uint16_t length)
{
  ecoap_put_option_string(value, length, eCoAP_OPTION_Location_Path);
}

void ecoap_put_location_pathz(const char *value)
{
  ecoap_put_location_path(value, _strlen_(value));
}

void ecoap_put_uri_path(const char *value, uint16_t length)
{
  ecoap_put_option_encoded(value, length, eCoAP_OPTION_Uri_Path);
}

void ecoap_put_uri_pathz(const char *value)
{
  ecoap_put_uri_path(value, _strlen_(value));
}

void ecoap_put_content_format(uint16_t value)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Content_Format));
  ecoap_put_option_uint32(value, eCoAP_OPTION_Content_Format);
}

void ecoap_put_max_age(uint32_t value)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Max_Age));
  ecoap_put_option_uint32(value, eCoAP_OPTION_Max_Age);
}

void ecoap_put_uri_query(const char *value, uint16_t length)
{
  ecoap_put_option_encoded(value, length, eCoAP_OPTION_Uri_Query);
}

void ecoap_put_uri_queryz(const char *value)
{
  ecoap_put_uri_query(value, _strlen_(value));
}

void ecoap_put_accept(uint16_t value)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Accept));
  ecoap_put_option_uint32(value, eCoAP_OPTION_Accept);
}

void ecoap_put_location_query(const char *value, uint16_t length)
{
  ecoap_put_option_string(value, length, eCoAP_OPTION_Location_Query);
}

void ecoap_put_location_queryz(const char *value)
{
  ecoap_put_location_query(value, _strlen_(value));
}

void ecoap_put_proxy_uri(const char *value, uint16_t length)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Proxy_Uri));
  ecoap_put_option_string(value, length, eCoAP_OPTION_Proxy_Uri);
}

void ecoap_put_proxy_uriz(const char *value)
{
  ecoap_put_proxy_uri(value, _strlen_(value));
}

void ecoap_put_proxy_scheme(const char *value, uint16_t length)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Proxy_Scheme));
  ecoap_put_option_string(value, length, eCoAP_OPTION_Proxy_Scheme);
}

void ecoap_put_proxy_schemez(const char *value)
{
  ecoap_put_proxy_scheme(value, _strlen_(value));
}

void ecoap_put_size1(uint32_t value)
{
  assert(!ecoap_has_put_option(eCoAP_OPTION_Size1));
  ecoap_put_option_uint32(value, eCoAP_OPTION_Size1);
}

void ecoap_put_content(const void *data, uint16_t length)
{
  assert(ecoap_is_writing());
  if (length != 0) {
    struct outgitem *item;
    item = _outgitem_add_(&_current_outgoing_->content, length, 0);
    memcpy(item->data, data, length);
  }
}

void ecoap_put_contentz(const char *value)
{
  ecoap_put_content(value, _strlen_(value));
}

int ecoap_put_uri(const char *uri)
{
  bool secured;
  uint16_t idx, jdx, len;
  int sts;
  char *node, *service;
  struct addrinfo *addrinfo, hint;

  assert(ecoap_is_writing());
  assert(!ecoap_has_put_option(eCoAP_OPTION_Uri_Host));
  assert(!ecoap_has_put_option(eCoAP_OPTION_Uri_Port));
  assert(!ecoap_has_put_option(eCoAP_OPTION_Uri_Path));
  assert(!ecoap_has_put_option(eCoAP_OPTION_Uri_Query));

  /* check coap/coaps/scheme prefix */
  if ((uri[0]|' ') != 'c' || (uri[1]|' ') != 'o' || (uri[2]|' ') != 'a' || (uri[3]|' ') != 'p')
    return -1;
  secured = (uri[4]|' ') == 's';

  /* check the colon */
  idx = 4 + (int)secured;
  if (uri[idx++] != ':')
    return -2;

  /* check double slash */
  if (uri[idx++] != '/' || uri[idx++] != '/')
    return -3;

  /* parse the host */
  if (uri[idx] == '/' || uri[idx] == ':')
    return -4;
  if (uri[idx] == '[') {
    /* the literal ipv6 address */
    for (jdx = ++idx ; uri[idx] != 0 && uri[idx] != ']' ; idx++);
    if (uri[idx] == 0)
      return -5;
    len = idx++ - jdx;
  } else if ('0' <= uri[idx] && uri[idx] <= '9') {
    /* the literal ipv4 address */
    for (jdx = idx ; uri[idx] != 0 && uri[idx] != ':' && uri[idx] != '/' ; idx++);
    len = idx - jdx;
  } else {
    /* named */
    for (jdx = idx ; uri[idx] != 0 && uri[idx] != ':' && uri[idx] != '/' ; idx++);
    len = idx - jdx;
    ecoap_put_uri_host(&uri[jdx], len);
  }
  node = alloca(len + 1);
  memcpy(node, &uri[jdx], len);
  node[len] = 0;

  /* check the port */
  service = NULL;
  if (uri[idx] == ':') {
    for (jdx = ++idx ; '0' <= uri[idx] && uri[idx] <= '9' ; idx++);
    len = idx - jdx;
    if (len != 0) {
      service = alloca(len + 1);
      memcpy(service, &uri[jdx], len);
      service[len] = 0;
      ecoap_put_uri_port((uint16_t)atoi(service));
    }
  }
  if (service == NULL)
    service = secured ? eCoAP_DEFAULT_COAPS_SERVICE : eCoAP_DEFAULT_COAP_SERVICE;

  /* get the authority */
  memset(&hint, 0, sizeof hint);
  hint.ai_socktype   = SOCK_DGRAM;
  hint.ai_family     = AF_INET;        /* TODO not only IPV4 please */
  sts = getaddrinfo(node, service, &hint, &addrinfo);
  if (sts != 0)
    return -6;
  _address_set_(&_current_outgoing_->target, addrinfo->ai_addr, addrinfo->ai_addrlen);
  freeaddrinfo(addrinfo);

  /* scan the path components */
  if (uri[idx] == 0)
    return 0; /* ok by default */
  if (uri[idx] != '/')
    return -7;
  do {
    for(jdx = ++idx ; uri[idx] != 0 && uri[idx] != '/' && uri[idx] != '?' ; idx++)
      if (uri[idx] == '#')
        return -8;
    len = idx - jdx;
    ecoap_put_uri_path(&uri[jdx], len);
  } while(uri[idx] == '/');

  /* scan the query components */
  if (uri[idx] == 0)
    return 0;
  do {
    for(jdx = ++idx ; uri[idx] != 0 && uri[idx] != '&' ; idx++);
    len = idx - jdx;
    ecoap_put_uri_query(&uri[jdx], len);
  } while(uri[idx] == '&');
  return 0;
}

/********************* SECTION: sending **********************************/

/* linked list of outgones with head and tail */
static struct outgoing *_outgones_head_ = NULL;
static struct outgoing *_outgones_tail_ = NULL;
BARRIER(outgones)

/* search the outgone of 'msgid' and 'origin' */
static struct outgoing *_outgone_search_id_(uint16_t msgid, const struct address *origin)
{
  struct outgoing *result;
  LOCK(outgones)
  result = _outgones_head_;
  while (result != NULL && (msgid != _message_id_(result->message) || 0 != _address_compare_(origin, &result->target)))
    result = result->next;
  UNLOCK(outgones)
  return result;
}

/* search the outgone of 'token' og length 'toklen' and 'origin' */
static struct outgoing *_outgone_search_tok_(uint8_t toklen, const uint8_t *token, const struct address *origin)
{
  struct outgoing *result;
  LOCK(outgones)
  result = _outgones_head_;
  while (result != NULL && !(_message_class_(result->message) == eCoAP_CODE_CLASS_REQUEST && toklen == _message_token_len_(result->message) && 0 == memcmp(token, _message_token_(result->message), toklen) && 0 == _address_compare_(origin, &result->target)))
    result = result->next;
  UNLOCK(outgones)
  return result;
}

static void _outgones_sync_(const struct timespec *when)
{
  bool waitmore;
  struct outgoing *out;

  LOCK(outgones)
  out = _outgones_head_;
  while (out != NULL) {
    if ((out->flags & _OUT_AWAITING_) != 0) {
      assert(out->flags == _OUT_AWAITING_);
      waitmore = true;
      if (_timespec_leq_(&out->retransmit.expire, when)) {
        if (!_retransmit_next_(&out->retransmit)) {
          waitmore = false;
        } else if (_outgoing_send_(out) < 0) {
          /* send error ?! TODO */
          waitmore = false;
        }
      }
      if (waitmore)
        _timer1_lazy_add_(&out->retransmit.expire);
      else
        out->flags = _OUT_EXPIRED_;
    }
    out = out->next;
  }
  UNLOCK(outgones)
}

/* Sends the currently composed message. Records it in outgones if needed */
int ecoap_send()
{
  int sts;
  struct outgoing *outgoing;
  struct incoming *request;
  struct interface *interface;

  assert(ecoap_is_writing());

  /* get the message */
  outgoing = _current_outgoing_;
  _current_outgoing_ = NULL;

  /* prepare creation of the message */
  request = outgoing->request;
  if (request != NULL) {
    if (_message_type_(&request->message) == eCoAP_TYPE_CONFIRMABLE && (request->flags & _IN_CONFIRMED_) != 0)
      outgoing->type = eCoAP_TYPE_CONFIRMABLE;
    else {
      request->flags |= _IN_REPLIED_;
      request->reply = outgoing;
    }
  }

  /* create the message */
  _outgoing_message_(outgoing);

  /* try to send it */
  if (outgoing->target.length == 0) {
    errno = EINVAL;
    sts = -1;
  } else {
    interface = outgoing->interface != NULL ? outgoing->interface : _default_origin_ != NULL ? _default_origin_ : _interfaces_;
    /* TODO?  LOCK(interfaces) */
    if (interface == NULL) {
      errno = EINVAL;
      sts = -1;
    } else {
      /* send */
      outgoing->interface = interface;
      sts = _outgoing_send_(outgoing);
      if (sts >= 0 && (_message_type_(outgoing->message) == eCoAP_TYPE_CONFIRMABLE || _message_class_(outgoing->message) == eCoAP_CODE_CLASS_REQUEST)) {
        /* set the timer */
        if (_message_type_(outgoing->message) == eCoAP_TYPE_CONFIRMABLE)
          _retransmit_init_con_(&outgoing->retransmit);
        else
          _retransmit_init_non_(&outgoing->retransmit);
        _timer1_add_(&outgoing->retransmit.expire);
        outgoing->flags = _OUT_AWAITING_;
        outgoing->signaled = 0;
        outgoing->next = NULL;
        /* add to outgones */
        LOCK(outgones)
        assert((_outgones_head_ == NULL) == (_outgones_tail_ == NULL));
        if (_outgones_tail_ == NULL)
          _outgones_head_ = outgoing;
        else
          _outgones_tail_->next = outgoing;
        _outgones_tail_ = outgoing;
        UNLOCK(outgones)
        outgoing = NULL;
      }
    }
  }
  /* clears and free outgoing */
  if (outgoing) {
    _free_(outgoing->message);
    _free_(outgoing);
  }
  return sts;
}

/********************* SECTION: receiving ********************************/

/* receives the data from fd */
static void _receive_interface_(struct interface *interface, const struct timespec *when)
{
  ssize_t rsize;
  struct incoming *income;
  struct incoming *previous;
  struct outgoing *out;
  uint16_t receive_size = RECEIVE_SIZE;

  /* get buffer and address */
  income = _malloc_(receive_size + sizeof * income);

  /* receive the data */
  income->origin.length = MAX_ADDRESS_LENGTH;
  do {
    rsize = recvfrom(interface->sock, income->message.body, receive_size, MSG_TRUNC, &income->origin.addr_any, &income->origin.length);
  } while (rsize < 0 && errno == EINTR);

  /* error on receive ? */
  if (rsize < 0) {
    _free_(income);
    return;
  }

  /* message to big ? */
  if (rsize > receive_size) {
    income->message.length = receive_size;
    income->flags = _IN_OVERFLOWN_;
  } else {
    income = _realloc_(income, (size_t)rsize + (sizeof * income - eCoAP_MINIMAL_MESSAGE_LENGTH));
    income->message.length = (uint16_t)rsize;
    income->flags = 0;
  }
  /* trace the received message */
  if (_traces_(trace_received))
    _message_dump_(&income->message, _trace_file_, "receives ");

  /* check minimum length */
  if (rsize < eCoAP_MINIMAL_MESSAGE_LENGTH) {
    _free_(income); /* too small data */
    return;
  }
  /* check version */
  if (_message_version_(&income->message) != eCoAP_VERSION_1) {
    _free_(income); /* bad version of the protocol */
    return;
  }

  /* needed for reset / ack */
  income->interface = interface;

  /* is it a valid message ? */
  if (_message_valid_(&income->message) != 0) {
    /* invalid message */
    _income_reset_(income);
    _free_(income);
    return;
  }

  /* is it an empty request? */
  if (_message_code_(&income->message) == eCoAP_CODE_EMPTY) {
    /* yes, empty request. check the size */
    if (income->message.length != eCoAP_MINIMAL_MESSAGE_LENGTH) {
      /* bad size, message format error: [RFC] 4.1 & 4.2 */
      _income_reset_(income);
    } else {
      /* good size, NOTE: the token length is not checked */
      switch (_message_type_(&income->message)) {
        case eCoAP_TYPE_ACKNOWLEDGMENT:
          /* confirms message of id */
          out = _outgone_search_id_(_message_id_(&income->message), &income->origin);
          if (out != NULL)
            out->flags = (out->flags & ~(_OUT_AWAITING_|_OUT_EXPIRED_|_OUT_RESET_)) | _OUT_CONFIRMED_;
          break;
        case eCoAP_TYPE_RESET:
          /* resets message of id */
          out = _outgone_search_id_(_message_id_(&income->message), &income->origin);
          if (out != NULL)
            out->flags = _OUT_RESET_;
          break;
        default:
          /* ping? */
          _income_reset_(income);
          break;
      }
    }
    _free_(income);
    return;
  }

  /* check duplication */
  previous = _incames_search_(_message_id_(&income->message), &income->origin);
  if (previous != NULL) {
    /* income is a duplication of previous */
    if (previous->flags & _IN_RESET_)
      _income_reset_(income);
    else if (previous->flags & _IN_CONFIRMED_)
      _income_ack_(income);
    else if (previous->flags & _IN_REPLIED_)
      _outgoing_send_(previous->reply);
    _free_(income);
    return;
  }

  /* not an empty request, so safely check the class */
  switch (_message_class_(&income->message)) {

    case eCoAP_CODE_CLASS_REQUEST:
      income->reply = NULL;
      break;

    case eCoAP_CODE_CLASS_RESPONSE:
    case eCoAP_CODE_CLASS_CLIENT_ERROR:
    case eCoAP_CODE_CLASS_SERVER_ERROR:
      out = _outgone_search_tok_(_message_token_len_(&income->message), _message_token_(&income->message), &income->origin);
      if (out == NULL) {
        /* unexpected reply */
        _income_reset_(income);
        _free_(income);
        return;
      }
      out->flags = (out->flags & ~(_OUT_AWAITING_|_OUT_EXPIRED_|_OUT_RESET_)) | _OUT_REPLIED_;
      income->request = out;
      if (_message_type_(&income->message) == eCoAP_TYPE_CONFIRMABLE) {
        income->flags |= _IN_CONFIRMED_;
        _income_ack_(income);
      }
      break;

    default:
      /* reserved class: [RFC] 4.2 */
      _income_reset_(income);
      _free_(income);
      return;
  }

  /* compute the expiration */
  income->expire = *when;
  _timeout_add_(eCoAP_EXCHANGE_LIFETIME_256, &income->expire);
  _timer1_add_(&income->expire);

  /* add income to the list of incames as new */
  income->next = NULL;
  LOCK(incames)
  if (_incames_tail_ == NULL) {
    assert(_incames_ == NULL);
    assert(_incames_new_ == NULL);
    _incames_ = income;
    _incames_new_ = income;
    _incames_tail_ = income;
  } else {
    assert(_incames_ != NULL);
    assert(_incames_tail_->next == NULL);
    _incames_tail_->next = income;
    _incames_tail_ = income;
    if (_incames_new_ == NULL)
      _incames_new_ = income;
  }
  UNLOCK(incames)
}

static int _receive_(int timeout)
{
  int n;
  struct epoll_event epes[16];
  struct timespec now;

  assert(_init_done_);
  while (true) {
    n = epoll_pwait(_main_epfd_, epes, sizeof epes / sizeof * epes, timeout, NULL);
    if (n <= 0)
      return n;
    _timespec_now_(&now);
    while(n > 0)
      if (epes[--n].data.ptr != NULL)
        _receive_interface_(epes[n].data.ptr, &now);
      else {
        uint64_t count;
        read(_timer1_fd_, &count, sizeof count);
        _timer1_clear_();
        _incames_sync_(&now);
        _outgones_sync_(&now);
        _timer1_lazy_store_();
      }
    timeout = 0;
  }
}

int ecoap_wait_max(int max_timeout)
{
  int sts = _receive_(max_timeout);
  assert(sts <= 0);
  return sts;
}

int ecoap_wait()
{
  return ecoap_wait_max(-1);
}

void (*ecoap_on_request)() = NULL;

void (*ecoap_on_reply)(enum ecoap_event event, void *userdata) = NULL;

void ecoap_dispatch_events()
{
  struct incoming *in;
  struct outgoing *out;
  struct outgoing *prev;
  void (*on_reply)(enum ecoap_event, void *);
  uint8_t ds;
#if ECOAP_REENTRANT_DISPATCH
  static bool loop;
  loop = true;
#elif defined(NDEBUG)
# define loop true
#else
  static bool loop = false;
  assert(!loop);
  loop = true;
#endif

  assert(_current_incoming_ == NULL);

  /* dispatch requests and replies */
  LOCK(incames)
  in = _incames_new_;
  while (loop && in != NULL) {
    _incames_new_ = in->next;
    UNLOCK(incames)
    assert(_current_incoming_ == NULL);
    _current_incoming_ = in;
    if (_message_class_(&in->message) == eCoAP_CODE_CLASS_REQUEST) {
      if (ecoap_on_request == NULL)
        ecoap_forget();
      else {
        _rewind_(in);
        ecoap_on_request();
      }
    } else {
      on_reply = in->request->handler;
      if (on_reply == NULL)
        on_reply = ecoap_on_reply;
      if (on_reply == NULL)
        ecoap_forget();
      else {
        _rewind_(in);
        _current_incoming_ = in;
        on_reply(ecoap_event_reply, in->request->userdata);
      }
    }
    if (_current_incoming_ != NULL)
      ecoap_treated(); /* set treated by default */
    assert(_current_incoming_ == NULL);
    LOCK(incames)
    in = _incames_new_;
  }
  UNLOCK(incames)

  prev = NULL;
  LOCK(outgones)
  out = _outgones_head_;
  UNLOCK(outgones)
  while (loop && out != NULL) {
    ds = out->flags & ~out->signaled; /* removes the already signaled events */

    /* signal */
    out->signaled |= ds;

    if ((ds & (_OUT_EXPIRED_|_OUT_RESET_|_OUT_CONFIRMED_)) != 0) {
      on_reply = out->handler;
      if (on_reply == NULL)
        on_reply = ecoap_on_reply;
      if (on_reply != NULL) {
        if ((ds & _OUT_EXPIRED_) != 0)
          on_reply(ecoap_event_expired, out->userdata);
        if ((ds & _OUT_RESET_) != 0)
          on_reply(ecoap_event_reset, out->userdata);
        if ((ds & _OUT_CONFIRMED_) != 0)
          on_reply(ecoap_event_confirmed, out->userdata);
      }
    }

    if ((ds & (_OUT_EXPIRED_ | _OUT_RESET_ | _OUT_REMOVABLE_)) == 0) {
      prev = out;
      out = out->next;
    } else {
      LOCK(outgones)
      if (_outgones_tail_ == out)
        _outgones_tail_ = prev;
      if (prev == NULL) {
        _outgones_head_ = out->next;
        _free_(out->message);
        _free_(out);
        out = _outgones_head_;
      } else {
        prev->next = out->next;
        _free_(out->message);
        _free_(out);
        out = prev->next;
      }
      UNLOCK(outgones)
    }
  }

#if ECOAP_REENTRANT_DISPATCH
  loop = false;
#elif !defined(NDEBUG)
  loop = false;
#endif
}

/* Initialise the Coap  */
int ecoap_init()
{
  assert(!_init_done_);
  _main_epfd_ = epoll_create1(EPOLL_CLOEXEC);
  if (_main_epfd_ >= 0) {
    _timer1_fd_ = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
    if (_timer1_fd_ >= 0) {
      struct epoll_event epe;
      epe.events = EPOLLIN;
      epe.data.ptr = NULL;
      if (epoll_ctl(_main_epfd_, EPOLL_CTL_ADD, _timer1_fd_, &epe) >= 0) {
        _init_done_ = 1;
        _hazard_init_();
        return _main_epfd_;
      }
      close(_timer1_fd_);
    }
    close(_main_epfd_);
  }
  return -1;
}

/********************* SECTION: buffer ***********************************/

struct buffer {
  uint16_t pos;
  char buffer[1024];
};

static void _buffer_init_(struct buffer *buffer)
{
  buffer->pos = 0;
}

static void _buffer_flush_(struct buffer *buffer)
{
  if (buffer->pos)
    ecoap_put_content(buffer->buffer, buffer->pos);
  _buffer_init_(buffer);
}

static void _buffer_put_char_(struct buffer *buffer, char c)
{
  if (buffer->pos == sizeof buffer->buffer)
    _buffer_flush_(buffer);
  buffer->buffer[buffer->pos++] = c;
}

static void _buffer_put_string_(struct buffer *buffer, const char *s, uint16_t l)
{
  uint16_t p = buffer->pos;
  if (l + p <= sizeof buffer->buffer) {
    memcpy(buffer->buffer + p, s, l);
    buffer->pos = l + p;
  } else {
    ecoap_put_content(buffer->buffer, p);
    if (l < sizeof buffer->buffer) {
      memcpy(buffer->buffer, s, l);
      buffer->pos = l;
    } else {
      ecoap_put_content(s, l);
      _buffer_init_(buffer);
    }
  }
}

/********************* SECTION: CoRE *************************************/

struct handler {
  struct handler *next;
  void (*handler)(void *userdata);
  uint8_t code;
  uint8_t flags;
};

struct attrdesc {
  struct attrdesc *next;
  const char *name;
  uint16_t length;   /* TODO uint8 ? */
  bool quote;
};

struct attr {
  struct attr *next;
  struct attrdesc *desc;
  const char *value;
};

struct node {
  const char *name;
  uint16_t length;   /* TODO uint8 ? */
  uint8_t  locks;
  struct node *parent;
  struct node *sibling;
  struct node *children;
  struct handler *handlers;
  struct attr *attributes;
  void *userdata;
};

#define _LOCK_HANDLERS_    1
#define _LOCK_ATTRIBUTES_  2
#define _LOCK_CHILDREN_    4
#define _LOCK_USERDATA_    8
#define _LOCK_LOCKS_       16

#define _LOCK_MOST_        (_LOCK_HANDLERS_|_LOCK_ATTRIBUTES_|_LOCK_USERDATA_)
#define _LOCK_ALL_         (_LOCK_MOST_|_LOCK_CHILDREN_|_LOCK_LOCKS_)

enum attrop { attrop_unquoted, attrop_quoted, attrop_get };

static __thread struct node *_current_node_ = NULL;

static void _core_get_(void *userdata);

static struct handler _handler_core_get_ = {
  .next = NULL,
  .handler = _core_get_,
  .code = eCoAP_CODE_GET
};

static struct attrdesc  _attrdesc_anchor_   = { NULL,                 "anchor",   6, false };
static struct attrdesc  _attrdesc_ct_       = { &_attrdesc_anchor_,   "ct",       2, false };
static struct attrdesc  _attrdesc_hreflang_ = { &_attrdesc_ct_,       "hreflang", 8, false };
static struct attrdesc  _attrdesc_if_       = { &_attrdesc_hreflang_, "if",       2, false };
static struct attrdesc  _attrdesc_media_    = { &_attrdesc_if_,       "media",    5, false };
static struct attrdesc  _attrdesc_rel_      = { &_attrdesc_media_,    "rel",      3, false };
static struct attrdesc  _attrdesc_rev_      = { &_attrdesc_rel_,      "rev",      3, false };
static struct attrdesc  _attrdesc_rt_       = { &_attrdesc_rev_,      "rt",       2, false };
static struct attrdesc  _attrdesc_sz_       = { &_attrdesc_rt_,       "sz",       2, false };
static struct attrdesc  _attrdesc_title_    = { &_attrdesc_sz_,       "title",    5, false };
static struct attrdesc  _attrdesc_title_s_  = { &_attrdesc_title_,    "title*",   6, false };
static struct attrdesc  _attrdesc_type_     = { &_attrdesc_title_s_,  "type",     4, false };
static struct attrdesc *_attrdescs_         = &_attrdesc_type_;

static struct node _predefined_nodes_[] = {
  [0] = {
    .name = "",
    .length = 0,
    .locks = 0,
    .parent = NULL,
    .sibling = NULL,
    .children = &_predefined_nodes_[1],
    .handlers = NULL,
    .attributes = NULL,
    .userdata = NULL
    },
  [1] = {
    .name = ".well-known",
    .length = 11,
    .locks = _LOCK_MOST_,
    .parent = &_predefined_nodes_[0],
    .sibling = NULL,
    .children = &_predefined_nodes_[2],
    .handlers = NULL,
    .attributes = NULL,
    .userdata = NULL
    },
  [2] = {
    .name = "core",
    .length = 4,
    .locks = _LOCK_MOST_,
    .parent = &_predefined_nodes_[1],
    .sibling = NULL,
    .children = NULL,
    .handlers = &_handler_core_get_,
    .attributes = NULL,
    .userdata = NULL
    }
};

#define _ROOT_   (&_predefined_nodes_[0])

struct node *_core_path_(const char *path, bool create)
{
  struct node *item, *node;
  uint16_t l;

  assert(*path == '/');

  item = _ROOT_;
  for(;;) {
    while(*path == '/') path++;
    for (l = 0 ; path[l] != 0 && path[l] != '/' ; l++);
    if (l == 0)
      return item;
    node = item->children;
    while (node != NULL && (l != node->length || 0 != memcmp(path, node->name, l)))
      node = node->sibling;
    if (node == NULL) {
      if (!create || 0 != (item->locks & _LOCK_CHILDREN_))
        return NULL;
      node = _malloc_(sizeof * node);
      node->name = path;
      node->length = l;
      node->locks = 0;
      node->parent = item;
      node->sibling = item->children;
      node->handlers = NULL;
      node->attributes = NULL;
      node->userdata = NULL;
      item->children = node;
    }
    item = node;
    path += l;
  }
}

static struct attrdesc *_core_attr_(const char *name, uint16_t length, enum attrop op)
{
  struct attrdesc *result = _attrdescs_, **prev = &_attrdescs_;
  int c;

  while (result != NULL) {
    if (length <= result->length) {
      c = memcmp(result->name, name, length);
      if (c == 0 && length == result->length)
        return result;
    } else {
      c = memcmp(result->name, name, result->length);
      if (c == 0)
        break;
    }
    if (c < 0)
      break;
    prev = &result->next;
    result = result->next;
  }
  if (op == attrop_get)
    return NULL;

  result = _malloc_(sizeof * result);
  result->next = *prev;
  result->name = name;
  result->length = length;
  result->quote = op;
  *prev = result;
  return result;
}

struct search {
  struct attrdesc *attr;
  const char *value;
  uint16_t length;
  uint16_t count;
  struct buffer buffer;
};

static int16_t _core_match_href_rec_(struct node *item, const char *prefix, uint16_t length)
{
  int16_t lb, i;

  assert(item != NULL);
  if (item->parent == NULL)
    return 0;

  lb = _core_match_href_rec_(item->parent, prefix, length);
  assert(lb <= length);
  if (lb < 0)
    return lb;

  while (lb < length && prefix[lb] == '/')
    lb++;

  length -= lb;
  if (length == 0)
    return lb;

  prefix += lb;
  for (i = 0 ; i < length ; i++) {
    if (prefix[i] == '/')
      break;
    if (prefix[i] == '*' && i+1 == length)
      break;
  }
  if (0 != memcmp(prefix, item->name, i))
    return -1;

  if (i < length && prefix[i] == '*')
    return lb + i + 1;

  if (item->name[i] == 0)
    return lb + i;

  return -1;
}

static bool _core_match_href_(struct node *item, const char *prefix, uint16_t length)
{
  assert(item != NULL);
  if (length == 0)
    return true;
  return _core_match_href_rec_(item, prefix, length) >= 0;
}

static bool _core_match_attr_(struct node *item, struct attrdesc *desc, const char *prefix, uint16_t length)
{
  struct attr *attr = item->attributes;
  while (attr != NULL) {
    if (attr->desc == desc) {
      bool ext = length > 0 && prefix[length-1]=='*';
      if (0 == memcmp(prefix, attr->value, length - ext) && (ext || 0 == attr->value[length]))
        return true;
    }
    attr = attr->next;
  }
  return false;
}

static void _core_path_write_(struct buffer *buffer, struct node *item)
{
  if (item == _ROOT_)
    _buffer_put_char_(buffer, '/');
  else {
    if (item->parent != _ROOT_)
      _core_path_write_(buffer, item->parent);
    _buffer_put_char_(buffer, '/');
    _buffer_put_string_(buffer, item->name, item->length);
  }
}

static void _core_get_write_(struct buffer *buffer, struct node *item)
{
  struct attr *attr;
  bool quote;
  _buffer_put_char_(buffer, '<');
  _core_path_write_(buffer, item);
  _buffer_put_char_(buffer, '>');
  for (attr = item->attributes ; attr != NULL ; attr = attr->next) {
    _buffer_put_char_(buffer, ';');
    _buffer_put_string_(buffer, attr->desc->name, attr->desc->length);
    _buffer_put_char_(buffer, '=');
    quote = attr->desc->quote && attr->value[0] != '"';
    if (quote)
      _buffer_put_char_(buffer, '"');
    _buffer_put_string_(buffer, attr->value, _strlen_(attr->value));
    if (quote)
      _buffer_put_char_(buffer, '"');
  }
}

static void _core_get_rec_(struct search *search, struct node *item)
{
  while (item != NULL) {
    if (search->value == NULL || search->attr != NULL || _core_match_href_(item, search->value, search->length)) {
      if (item->handlers != NULL && item->attributes != NULL) {
        if (search->attr == NULL || _core_match_attr_(item, search->attr, search->value, search->length)) {
          if (search->count++ != 0)
            _buffer_put_char_(&search->buffer, ',');
          _core_get_write_(&search->buffer, item);
        }
      }
      if (item->children != NULL)
        _core_get_rec_(search, item->children);
    }
    item = item->sibling;
  }
}

static void _core_get_(void *userdata)
{
  struct search search;

  search.attr = NULL;
  search.value = NULL;
  search.count = 0;
  if (ecoap_select_uri_query()) {
    struct ecoap_data d = ecoap_get_data();
    uint16_t l = 0;
    while (l < d.length && (char)(d.data[l]) != '=') l++;
    if (l < d.length) {
      if (l == 4 && 0 == memcmp(d.data, "href", 4)) {
        search.value = (const char*)(d.data + 5);
        search.length = d.length - 5;
      } else {
        search.attr = _core_attr_((const char*)d.data, l, attrop_get);
        if (search.attr != NULL) {
          search.value = (const char*)(d.data + l + 1);
          search.length = d.length - l - 1;
        }
      }
    }
  }

  ecoap_reply_content(); 
  _buffer_init_(&search.buffer);
  _core_get_rec_(&search, _ROOT_);
  _buffer_flush_(&search.buffer);
  ecoap_send();
}

void ecoap_core_dispatch()
{
  struct node *item;
  bool set;
  uint8_t code;
  struct handler *handler;

  assert(ecoap_is_request());

  /* search the node starting at root */
  item = _ROOT_;
  set = ecoap_select_uri_path();
  while (set) {
    if (ecoap_get_length() != 0) {
      item = item->children;
      while (item != NULL && ecoap_comparez(item->name) != 0)
        item = item->sibling;
      if (item == NULL) {
        /* path not found */
        ecoap_reply_not_found();
        ecoap_send();
        ecoap_forget();
        return;
      }
    }
    set = ecoap_select_next_sibling();
  }

  /* found, search the callback */
  code = ecoap_get_code();
  handler = item->handlers;
  if (handler == NULL && item->attributes == NULL) {
    /* TODO list the resources here */
    ecoap_reply_method_not_allowed();
    ecoap_send();
    ecoap_forget();
    return;
  }
  while (handler != NULL && handler->code != code)
    handler = handler->next;
  if (handler != NULL)
    /* handler found, call it */
    handler->handler(item->userdata);
  else {
    ecoap_reply_method_not_allowed();
    ecoap_send();
    ecoap_forget();
  }
}

bool ecoap_core_path(const char *path)
{
  _current_node_ = path == NULL ? NULL : _core_path_(path, true);
  return path == NULL || _current_node_ != NULL;
}

void ecoap_core_on(uint8_t code, void (*handler)(void *userdata))
{
  struct handler *item;

  assert(_current_node_ != NULL);

  if (0 != (_current_node_->locks & _LOCK_HANDLERS_))
    return;

  item = _current_node_->handlers;
  while (item != NULL && item->code != code)
    item = item->next;
  if (item == NULL) {
    item = _malloc_(sizeof * item);
    item->code = code;
    item->next = _current_node_->handlers;
    _current_node_->handlers = item;
  }
  item->handler = handler;
}

void ecoap_core_on_get(void (*handler)(void *userdata))
{
  ecoap_core_on(eCoAP_CODE_GET, handler);
}

void ecoap_core_on_post(void (*handler)(void *userdata))
{
  ecoap_core_on(eCoAP_CODE_POST, handler);
}

void ecoap_core_on_put(void (*handler)(void *userdata))
{
  ecoap_core_on(eCoAP_CODE_PUT, handler);
}

void ecoap_core_on_delete(void (*handler)(void *userdata))
{
  ecoap_core_on(eCoAP_CODE_DELETE, handler);
}

void ecoap_core_userdata(void *userdata)
{
  assert(_current_node_ != NULL);

  if (0 != (_current_node_->locks & _LOCK_USERDATA_))
    return;

  _current_node_->userdata = userdata;
}

static void _core_add_attr_(const char *value, struct attrdesc *desc)
{
  struct attr *attr;

  assert(_current_node_ != NULL);

  if (0 != (_current_node_->locks & _LOCK_ATTRIBUTES_))
    return;

  attr = _current_node_->attributes;
  while (attr != NULL && desc != attr->desc)
    attr = attr->next;
  if (attr == NULL) {
    attr = _malloc_(sizeof * attr);
    attr->desc = desc;
    attr->next = _current_node_->attributes;
    _current_node_->attributes = attr;
  }
  attr->value = value;
}

void ecoap_core_attr(const char *value, const char *name, bool quote)
{
  assert(memcmp(name, "href", 5) != 0);
  _core_add_attr_(value, _core_attr_(name, _strlen_(name), quote));
}

void ecoap_core_anchor(const char *value)
{
  _core_add_attr_(value, &_attrdesc_anchor_);
}

void ecoap_core_ct(const char *value)
{
  _core_add_attr_(value, &_attrdesc_ct_);
}

void ecoap_core_hreflang(const char *value)
{
  _core_add_attr_(value, &_attrdesc_hreflang_);
}

void ecoap_core_if(const char *value)
{
  _core_add_attr_(value, &_attrdesc_if_);
}

void ecoap_core_media(const char *value)
{
  _core_add_attr_(value, &_attrdesc_media_);
}

void ecoap_core_rel(const char *value)
{
  _core_add_attr_(value, &_attrdesc_rel_);
}

void ecoap_core_rev(const char *value)
{
  _core_add_attr_(value, &_attrdesc_rev_);
}

void ecoap_core_rt(const char *value)
{
  _core_add_attr_(value, &_attrdesc_rt_);
}

void ecoap_core_sz(const char *value)
{
  _core_add_attr_(value, &_attrdesc_sz_);
}

void ecoap_core_title(const char *value)
{
  _core_add_attr_(value, &_attrdesc_title_);
}

void ecoap_core_title_star(const char *value)
{
  _core_add_attr_(value, &_attrdesc_title_s_);
}

void ecoap_core_type(const char *value)
{
  _core_add_attr_(value, &_attrdesc_type_);
}

/********************* END ***********************************************/
/* vim: set sts=2 sta sw=2 et ts=8 wrap: */ 

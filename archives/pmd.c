#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

int main(int ac, char **av)
{
  uint16_t v=1, i=0;
  do {
    printf("%d %d %d\n", (int)i, (int)v, (int)(v>>8));
    v = 27621 * v + 20723;
  } while(++i);
  return 0;
}


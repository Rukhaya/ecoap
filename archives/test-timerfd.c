#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <netdb.h>
#include <poll.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <netinet/in.h>
#include <netinet/ip.h>


int main()
{
  struct itimerspec its;
  int n;
  struct pollfd fds;
  uint64_t z;

  fds.events = POLLIN;
  fds.revents = (short)-1;
  fds.fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
  its.it_interval.tv_sec = 0;
  its.it_interval.tv_nsec = 0;
  clock_gettime(CLOCK_MONOTONIC, &its.it_value);
  its.it_value.tv_sec--;
  timerfd_settime(fds.fd, TFD_TIMER_ABSTIME, &its, NULL);
#if 0
  clock_gettime(CLOCK_MONOTONIC, &its.it_value);
#else
  its.it_value.tv_sec++;
#endif
  n = poll(&fds, 1, 1000);
  clock_gettime(CLOCK_MONOTONIC, &its.it_interval);
  its.it_interval.tv_sec -= its.it_value.tv_sec;
  its.it_interval.tv_nsec -= its.it_value.tv_nsec;
  if (its.it_interval.tv_nsec < 0) {
    its.it_interval.tv_nsec += 1000000;
    its.it_interval.tv_sec++;
  }
  if (n != 1) {
    printf("failed %d %m\n", n);
    return 1;
  }
  read(fds.fd, &z, sizeof z);
  printf("success %d/%d %lu %d.%09ld\n", (int)fds.events, (int)fds.revents, z, (int)its.it_interval.tv_sec, its.it_interval.tv_nsec);
  return 0;
}

